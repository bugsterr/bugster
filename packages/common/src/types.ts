export interface User {
    _id: string,
    username: string,
    isAdmin?: boolean,
    fullName?: string,
    coinsSpent: number,
}

export interface Experience {
    _id: string,
    userId: string,
    type: string,
    value: number
}

export interface Bug {
    _id: string,
    title: string,
    description: string,
    type: string,
    testedApp: string,
    appVersion: string,
    deviceInfo: string,
    creationDate: Date,
    creator: string,
    status: 'reviewing' | 'rejected' | 'accepted',
    comments: number,
}

export interface Attachment {
    _id: string,
    attachments: Buffer[],
    relatedBug: string,
    owner: string,
}

export interface Comment {
    _id: string,
    text: string,
    creationDate: string,
    creator: string,
    relatedBug: string,
}

export interface Product {
    _id: string,
    picture: string,
    title: string,
    description: string,
    price: number,
}

export interface Order {
    product: string,
    buyer: string,
}

export interface Guild {
    _id: string,
    name: string,
    members: string[],
    creator: string,
    channel: string,
    hasMultiplier?: boolean,
}

export interface Template {
    _id: string,
    testedApp: string,
    appVersion: string,
    deviceInfo: string,
    creator: string,
}

import React from 'react';

import { Tabs } from 'antd';

const { TabPane: AntdTabPane } = Tabs;

export interface TabPaneProps {
    key: string;
    icon?: React.ReactNode;
    children?: React.ReactNode;
}

const TabPane: React.FunctionComponent<TabPaneProps> = ({ key, icon, children }) => {
    throw new Error("Don't render this directly!");
};

export type TabPaneType = typeof TabPane & { mapToOrigin: (tab: React.ReactElement<TabPaneProps>) => React.ReactNode };

// Hack because of strange inner manipulations of Ant, can't render.
(TabPane as TabPaneType).mapToOrigin = (tab) => {
    return (
        <AntdTabPane
            key={tab.key}
            tab={
                <span>
                    {tab.props.icon}
                    {tab.props.children}
                </span>
            }
        />
    );
};

export default TabPane as TabPaneType;

import React, { useCallback, useState, KeyboardEvent } from 'react';
import classNames from 'classnames';

import './NewComment.scss';

import { TextArea } from 'Elements/Form';
import { Alert, Button } from 'antd';
import { SendOutlined } from '@ant-design/icons';
import { BugsService } from 'api/bugs-service';
import { Comment } from 'common';

export interface NewCommentProps {
    bugId: string;
    onNewComment: (comment: Comment) => void;
    className?: string;
}

const NewComment: React.FunctionComponent<NewCommentProps> = ({ bugId, className, onNewComment }) => {
    const [loading, setLoading] = useState(false);
    const [text, setText] = useState('');
    const [error, setError] = useState<string>();

    const submit = useCallback(() => {
        if (text.replace(/\s*/, '') === '') return;

        const toSend = text;
        setError(undefined);
        setText('');
        setLoading(true);

        BugsService.comment({ text: toSend, relatedBug: bugId })
            .then((comment) => {
                setLoading(false);
                onNewComment(comment);
            })
            .catch((error) => {
                setLoading(false);
                setError('' + error);
            });
    }, [bugId, text, onNewComment]);

    const onKeyDown = useCallback(
        (e: KeyboardEvent) => {
            if (e.key === 'Enter' && e.ctrlKey) {
                e.preventDefault();
                submit();
            }
        },
        [submit],
    );

    return (
        <div className={classNames('NewComment', className)}>
            <div className="NewComment-main">
                <TextArea
                    className="NewComment-input"
                    placeholder="Введите комментарий..."
                    rows={3}
                    disabled={loading}
                    value={text}
                    onChange={(e) => setText(e.target.value)}
                    onKeyDown={onKeyDown}
                />
                <Button type="text" icon={<SendOutlined />} loading={loading} onClick={submit} />
            </div>
            {error ? <Alert className="mt-8" message={error} type="error" closable /> : null}
        </div>
    );
};

export default NewComment;

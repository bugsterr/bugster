import React, { useState } from 'react';

import { Row, Col, Tabs, Divider } from 'antd';

import Typography from 'Elements/Typography/Typography';
import TabPane, { TabPaneProps } from 'Elements/TabPane/TabPane';

import './Page.scss';

export interface PageProps {
    title: string;
    tabs: React.ReactElement<TabPaneProps>[];
    right?: React.ReactNode;
    sidebar?: React.ReactNode;
    main: {
        key: string;
        render: React.ReactNode;
    }[];
    children?: never;
}

const Page: React.FunctionComponent<PageProps> = ({ title, tabs, right, sidebar, main }) => {
    const [tab, setTab] = useState(main.length > 0 ? main[0].key : '');

    return (
        <div className="Page">
            <Row style={{ height: 70 }} align="middle">
                <Col span={8}>
                    <Typography size="h1">{title}</Typography>
                </Col>
                <Col span={16} className="Page-right">
                    <Tabs activeKey={tab} onChange={setTab}>
                        {tabs.map(TabPane.mapToOrigin)}
                    </Tabs>
                    {right}
                </Col>
            </Row>
            <Divider className="mb-20" />
            <Row align="top">
                <Col span={8} style={{ paddingRight: 20 }}>
                    {sidebar}
                </Col>
                <Col span={16}>{main.find((c) => c.key === tab)?.render ?? null}</Col>
            </Row>
        </div>
    );
};

export default Page;

import React from 'react';
import Typography from 'Elements/Typography/Typography';

import './Params.scss';

export interface ParamsProps {
    data: [string, string][];
}

const Params: React.FunctionComponent<ParamsProps> = ({ data }) => {
    return (
        <div className="Params">
            <div className="Params-left">
                {data
                    .map((entry) => entry[0] + ':')
                    .map((text) => (
                        <Typography size="body2">{text}</Typography>
                    ))}
            </div>
            <div className="Params-right">
                {data
                    .map((entry) => entry[1])
                    .map((text) => (
                        <Typography size="body2">{text}</Typography>
                    ))}
            </div>
        </div>
    );
};

export default Params;

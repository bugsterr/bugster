import React, { useCallback, useState } from 'react';

import './BugAdmin.scss';
import { Bug } from 'common';
import { Button, Col, Modal, Row } from 'antd';
import { BugsService } from 'api/bugs-service';

export interface CommentProps {
    bug: Bug;
}

const BugAdmin: React.FunctionComponent<CommentProps> = ({ bug }) => {
    const [loading, setLoading] = useState(false);

    const resolve = useCallback(
        (accepted: boolean) => {
            setLoading(true);
            BugsService.resolve({ id: bug._id, status: accepted })
                .then(() => {
                    setLoading(false);
                    window.location.reload();
                })
                .catch((error) => {
                    setLoading(false);
                    console.log(error);
                    Modal.error({
                        content: 'Не удалось зарезолвить баг',
                    });
                });
        },
        [bug._id],
    );

    return (
        <Row gutter={8} className="mt-16">
            <Col span={12}>
                <Button className="BugAdmin-accept" type="primary" onClick={() => resolve(true)} loading={loading}>
                    ПРИНЯТЬ
                </Button>
            </Col>
            <Col span={12}>
                <Button className="BugAdmin-decline" type="primary" onClick={() => resolve(false)} loading={loading}>
                    ОТКЛОНИТЬ
                </Button>
            </Col>
        </Row>
    );
};

export default BugAdmin;

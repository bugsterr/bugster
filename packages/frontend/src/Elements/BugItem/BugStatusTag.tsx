import React from 'react';
import HourglassOutlined from '@ant-design/icons/HourglassOutlined';
import DeploymentUnitOutlined from '@ant-design/icons/DeploymentUnitOutlined';
import CloseOutlined from '@ant-design/icons/CloseOutlined';
import { Tag } from 'antd';

interface BugStatusProps {
    status: 'reviewing' | 'rejected' | 'accepted';
}

export const BugStatusTag: React.FunctionComponent<BugStatusProps> = ({ status }: BugStatusProps) => {
    const getStatus = (status: 'reviewing' | 'rejected' | 'accepted') => {
        switch (status) {
            case 'reviewing':
                return ['На проверке', <HourglassOutlined style={{ marginLeft: 5 }} />];
            case 'accepted':
                return ['Принято', <DeploymentUnitOutlined style={{ marginLeft: 5 }} />];
            case 'rejected':
                return ['Отклонено', <CloseOutlined style={{ marginLeft: 5 }} />];
            default:
                return ['На проверке', <HourglassOutlined style={{ marginLeft: 5 }} />];
        }
    };

    const getStyles = (status: 'reviewing' | 'rejected' | 'accepted'): React.CSSProperties => {
        switch (status) {
            case 'reviewing':
                return {
                    textTransform: 'uppercase',
                    color: 'white',
                    height: 21,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: '10px',
                    lineHeight: 'normal',
                    fontWeight: 'bold',
                    background: '#F2C94C',
                };
            case 'rejected':
                return {
                    textTransform: 'uppercase',
                    color: 'white',
                    height: 21,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: '10px',
                    lineHeight: 'normal',
                    fontWeight: 'bold',
                    background: '#EB5757',
                };
            case 'accepted':
                return {
                    textTransform: 'uppercase',
                    color: 'white',
                    height: 21,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: '10px',
                    lineHeight: 'normal',
                    fontWeight: 'bold',
                    background: '#6FCF97',
                };
            default:
                return {};
        }
    };

    const content = getStatus(status);
    const styles = getStyles(status);

    return <Tag style={styles}>{content}</Tag>;
};

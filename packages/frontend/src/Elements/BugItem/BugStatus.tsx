import Typography from 'Elements/Typography/Typography';
import HourglassOutlined from '@ant-design/icons/HourglassOutlined';
import DeploymentUnitOutlined from '@ant-design/icons/DeploymentUnitOutlined';
import CloseOutlined from '@ant-design/icons/CloseOutlined';
import React from 'react';
import { Col, Row } from 'antd';

export interface BugStatusProps {
    status: 'reviewing' | 'rejected' | 'accepted';
}

const BugStatus: React.FunctionComponent<BugStatusProps> = ({ status }: BugStatusProps) => {
    const getContent = (status: 'reviewing' | 'rejected' | 'accepted') => {
        switch (status) {
            case 'reviewing':
                return ['На проверке', <HourglassOutlined />, '#F2C94C'];
            case 'accepted':
                return ['Принято', <DeploymentUnitOutlined />, '#6FCF97'];
            case 'rejected':
                return ['Отклонено', <CloseOutlined />, '#EB5757'];
            default:
                return ['На проверке', <HourglassOutlined />, '#F2C94C'];
        }
    };

    const content = getContent(status);

    return (
        <Col style={{ color: content[2] as string, display: 'flex', alignItems: 'center' }}>
            <Row align="middle">
                <Typography size="label" style={{ marginRight: 5 }}>
                    {content[0]}
                </Typography>
                {content[1]}
            </Row>
        </Col>
    );
};

export default BugStatus;

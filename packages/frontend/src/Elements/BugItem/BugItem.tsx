import React, { useState } from 'react';
import moment from 'moment';
import { Row, Col, Space } from 'antd';
import Typography from 'Elements/Typography/Typography';
import CommentOutlined from '@ant-design/icons/CommentOutlined';
import BugModal from './BugModal';

import './BugItem.scss';
import { Bug } from 'common';
import { useUsers } from 'Users';
import BugStatus from './BugStatus';
import { declOfNum } from 'declOfNum';
import { BugTypeTag } from './BugTypeTag';

export interface Tags {
    // need rework
    name: string;
}

export interface BugItemProps extends React.HTMLAttributes<HTMLDivElement> {
    bug: Bug;
    span?: number;
}

const BugItem: React.FunctionComponent<BugItemProps> = ({ bug, span, ...props }: BugItemProps) => {
    const spanWidth = span || 24;

    const [open, setOpen] = useState(false);
    const date = moment(bug.creationDate);
    const { getUser } = useUsers();
    const creator = getUser(bug.creator);

    const loadModal = (value: boolean) => {
        if (value) document.body.style.overflow = 'hidden';
        else document.body.style.overflow = 'unset';
        setOpen(value);
    };

    const comments =
        spanWidth > 12
            ? bug.comments + ' ' + declOfNum(bug.comments, ['Комментарий', 'Комментария', 'Комментариев'])
            : bug.comments.toString();

    return (
        <React.Fragment>
            <BugModal bug={bug} visible={open} onClose={() => loadModal(false)} />
            <Col span={spanWidth} onClick={() => loadModal(true)}>
                <div className="BugItem">
                    <Row align="middle">
                        <Col flex="auto">
                            <Typography size="body1" style={{ fontWeight: 600 }}>
                                {bug.title}
                            </Typography>
                        </Col>
                        <BugStatus status={bug.status} />
                    </Row>
                    <Row align="middle" style={{ margin: '5px 0' }} gutter={[0, 8]}>
                        <Space>
                            <BugTypeTag type={bug.type} />
                            {/* <Col>{bug.type}</Col> */}
                            {/* {bug.tags
                            ? tags.map((tag, index) => {
                                  return (
                                      <Col>
                                          <Tag key={index}>{tag.name}</Tag>
                                      </Col>
                                  );
                              })
                            : null} */}
                        </Space>
                    </Row>
                    <Row align="middle">
                        <Col flex="auto" style={{ color: '#90ACB5' }}>
                            <Space>
                                <CommentOutlined />
                                <Typography size="body2">{comments}</Typography>
                            </Space>
                        </Col>
                        <Col>
                            <Space>
                                <Typography size="body2" className="dark-blue-text">
                                    {creator?.username}
                                </Typography>
                                <Typography size="body2" className="tile-blue-text">
                                    /
                                </Typography>
                                <Typography size="body2" className="tile-blue-text">
                                    {date.format('YYYY-MM-DD HH:mm:ss')}
                                </Typography>
                            </Space>
                        </Col>
                    </Row>
                </div>
            </Col>
        </React.Fragment>
    );
};

export default BugItem;

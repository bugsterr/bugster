import React from 'react';
import { Tag } from 'antd';

interface BugTypesProps {
    type: string;
}

export const BugTypeTag: React.FunctionComponent<BugTypesProps> = ({ type }: BugTypesProps) => {
    const getType = (type: string) => {
        switch (type) {
            case 'design':
                return ['Дизайн'];
            case 'functional':
                return ['Функционал'];
            case 'vulnerability':
                return ['Уязвимости'];
            default:
                return [type];
        }
    };

    const getStyles = (type: string): React.CSSProperties => {
        switch (type) {
            case 'design':
                return {
                    textTransform: 'uppercase',
                    color: 'white',
                    height: 21,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: '10px',
                    fontWeight: 'bold',
                    background: 'linear-gradient(270deg, #BA26F1 0%, #E051AF 100%)',
                };
            case 'functional':
                return {
                    textTransform: 'uppercase',
                    color: 'white',
                    height: 21,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: '10px',
                    fontWeight: 'bold',
                    background: 'linear-gradient(89.9deg, #1BD2FA 0.09%, #1053FF 99.92%)',
                };
            case 'vulnerability':
                return {
                    textTransform: 'uppercase',
                    color: 'white',
                    height: 21,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: '10px',
                    fontWeight: 'bold',
                    background: 'linear-gradient(90deg, #FF9F1C 0%, #FF3E01 100%)',
                };
            default:
                return {};
        }
    };

    const content = getType(type);
    const styles = getStyles(type);

    return <Tag style={styles}>{content}</Tag>;
};

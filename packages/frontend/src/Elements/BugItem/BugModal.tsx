import React, { useCallback, useState } from 'react';

import { Alert, Col, Divider, Image, Modal, Row, Skeleton, Space } from 'antd';
import { SafetyOutlined } from '@ant-design/icons';
import { Bug, Comment as CommentType } from 'common';

import './BugModal.scss';

import Typography from 'Elements/Typography/Typography';
import Params from 'Elements/Params/Params';
import Comment from 'Elements/Comment/Comment';
import { useUsers } from 'Users';
import { useData } from 'api/use-data';
import { BugsService } from 'api/bugs-service';
import NewComment from 'Elements/NewComment/NewComment';
import times from 'lodash/times';
import { BugTypeTag } from './BugTypeTag';
import { BugStatusTag } from './BugStatusTag';
import BugAdmin from './BugAdmin';

export interface BugProps {
    visible: boolean;
    onClose: () => void;
    bug: Bug;
}

export interface InnerBugProps {
    bug: Bug;
}

const BugModalInner: React.FunctionComponent<InnerBugProps> = ({ bug }) => {
    const { getUser, myUser } = useUsers();

    const [localComments, setLocalComments] = useState<CommentType[]>([]);

    const addLocalComment = useCallback((comment: CommentType) => {
        setLocalComments((comments) => [...comments, comment]);
    }, []);

    const comments = useData(BugsService.getComments, { relatedBug: bug._id });
    const attachments = useData(BugsService.getAttachment, { relatedBug: bug._id });

    const creator = getUser(bug.creator);
    const deviceInfo = bug.deviceInfo.split(';');

    return (
        <>
            <Space>
                <BugTypeTag type={bug.type} />
                <BugStatusTag status={bug.status} />
            </Space>
            <Typography size="h1" className="mt-20">
                {bug.title}
            </Typography>
            <Row gutter={24} className="mt-20">
                <Col span={16}>
                    <Typography size="h3" className="mb-12">
                        Шаги воспроизведения
                    </Typography>
                    <Typography size="body1" className="mb-24">
                        {bug.description}
                    </Typography>
                    <Space className="mb-24">
                        {attachments.map({
                            loading: () => (
                                <>
                                    {times(4, (i) => (
                                        <Skeleton.Image key={i} style={{ width: 64, height: 64 }} />
                                    ))}
                                </>
                            ),
                            error: (error) => (
                                <Typography size="body1" className="dark-blue-color">
                                    Скриншоты не доступны
                                </Typography>
                            ),
                            success: (attachments) => (
                                <>
                                    {attachments.attachments.map((item) => {
                                        const url = Buffer.from(item).toString('ascii');
                                        return <Image width={64} height={64} className="BugModal-image" src={url} />;
                                    })}
                                </>
                            ),
                        })}
                    </Space>
                    <Typography size="h3" className="mb-12">
                        Параметры окружения
                    </Typography>
                    <Params
                        data={[
                            ['Продукт', bug.testedApp],
                            ['Платформа', deviceInfo[0]],
                            ['Операционная система', deviceInfo[1]],
                            ['Версия приложения', bug.appVersion],
                            ['Название устройства', deviceInfo[2]],
                        ]}
                    />
                </Col>
                <Col span={8} className="BugModal-right">
                    {creator ? (
                        <>
                            <Space>
                                <Typography size="h4" className="BugModal-user dark-blue-color">
                                    {creator.username}
                                </Typography>
                                <SafetyOutlined className="light-blue-color" />
                            </Space>
                            <Typography size="body1" className="tile-blue-color">
                                Сотрудник банка «Открытие»
                            </Typography>
                        </>
                    ) : (
                        <>
                            <Space className="mb-4">
                                <Skeleton.Input size="small" style={{ width: '120px' }} active />
                                <Skeleton.Avatar size="small" shape="square" active />
                            </Space>
                            <Skeleton.Input size="small" style={{ width: '180px' }} active />
                        </>
                    )}
                    {myUser?.isAdmin && bug.status === 'reviewing' ? <BugAdmin bug={bug} /> : null}
                </Col>
            </Row>
            <Divider className="mt-20 mb-20" />
            <Typography size="h3" className="mb-20">
                Комментарии
            </Typography>
            <Row>
                <Col span={16}>
                    <NewComment bugId={bug._id} className="mb-20" onNewComment={addLocalComment} />
                    {comments.map({
                        loading: () => (
                            <>
                                {[...Array(bug.comments)].map((idx) => (
                                    <Skeleton key={idx} active avatar paragraph={{ rows: 2 }} />
                                ))}
                            </>
                        ),
                        error: (error) => <Alert message={error} type="error" closable />,
                        success: (comments) => (
                            <>
                                {comments.concat(localComments).map((comment, idx) => (
                                    <Comment key={idx} comment={comment} />
                                ))}
                            </>
                        ),
                    })}
                </Col>
            </Row>
        </>
    );
};

const BugModal: React.FunctionComponent<BugProps> = ({ visible, onClose, bug }) => {
    return (
        <Modal
            visible={visible}
            onCancel={onClose}
            footer={null}
            className="BugModal"
            width={926}
            destroyOnClose={true}
        >
            <BugModalInner bug={bug} />
        </Modal>
    );
};

export default BugModal;

import React, { useState } from 'react';

import { Space, Image, Button, Modal } from 'antd';

import Typography from 'Elements/Typography/Typography';
import { ShoppingCartOutlined } from '@ant-design/icons';

import './Product.scss';

import { Product as ProductType } from 'common';
import { declOfNum } from 'declOfNum';

export interface ProductProps {
    product: ProductType;
    buy?: (productId: string) => Promise<void>;
}

const Product: React.FunctionComponent<ProductProps> = ({ product, buy }) => {
    const [loading, setLoading] = useState(false);

    const innerBuy = () => {
        if (buy) {
            setLoading(true);
            buy(product._id)
                .then(() => {
                    setLoading(false);
                    window.location.reload();
                })
                .catch((error) => {
                    setLoading(false);
                    console.log(error);
                    Modal.error({
                        content: 'Не удалось купить продукт',
                    });
                });
        }
    };

    const badge = buy ? (
        <Typography size="body2" className="dark-blue-color">
            {product.price} {declOfNum(product.price, ['балл', 'балла', 'баллов'])}
        </Typography>
    ) : (
        <Space>
            <ShoppingCartOutlined style={{ lineHeight: '17px' }} className="green-2-color" />
            <Typography size="body2" className="green-2-color">
                Куплено
            </Typography>
        </Space>
    );

    return (
        <Space className="Product" direction="vertical" align="center">
            <div className="Product-image">
                <div className="Product-image-dummy" />
                <Image className="Product-image-img" width="100%" src={product.picture} />
                <div className="Product-image-badge">{badge}</div>
            </div>
            <Typography size="body1" className="dark-blue-color">
                {product.title}
            </Typography>
            {buy ? (
                <Button type="primary" className="Product-buy" onClick={innerBuy} loading={loading}>
                    КУПИТЬ
                </Button>
            ) : null}
        </Space>
    );
};

export default Product;

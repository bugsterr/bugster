import React from 'react';
import moment from 'moment';

import './Comment.scss';
import { Comment as CommentType } from 'common';
import { Comment as AntdComponent, Skeleton, Tooltip } from 'antd';
import Avatar from 'antd/lib/avatar/avatar';
import Typography from 'Elements/Typography/Typography';
import { useUsers } from 'Users';

export interface CommentProps {
    comment: CommentType;
}

const Comment: React.FunctionComponent<CommentProps> = ({ comment }) => {
    const { getUser } = useUsers();

    const creator = getUser(comment.creator);

    const time = moment(comment.creationDate);

    return (
        <AntdComponent
            className="Comment"
            author={
                creator ? (
                    <Typography size="body1" className="Comment-author-name" forceSpan>
                        {creator.username}
                    </Typography>
                ) : (
                    <Skeleton.Input size="small" style={{ width: '80px' }} />
                )
            }
            avatar={<Avatar src="/images/avatar.png" alt="Avatar" />}
            content={comment.text}
            datetime={
                <Tooltip title={time.format('YYYY-MM-DD HH:mm:ss')}>
                    <Typography size="body2" className="Comment-author-time" forceSpan>
                        {time.format('YYYY-MM-DD HH:mm:ss')}
                    </Typography>
                </Tooltip>
            }
        />
    );
};

export default Comment;

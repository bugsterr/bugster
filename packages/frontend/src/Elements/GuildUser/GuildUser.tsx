import React from 'react';
import './GuildUser.scss';
import { Col, Row, Tag } from 'antd';
import Avatar from 'antd/lib/avatar/avatar';
import Typography from 'Elements/Typography/Typography';
import { useUsers } from 'Users';

interface UserProps {
    id: string;
    description: string;
}

const GuildUser = ({ id, description }: UserProps) => {
    const { getUser } = useUsers();

    const user = getUser(id);

    return (
        <Col span={24}>
            <Row gutter={[8, 8]}>
                <Col span={24}>
                    <Row>
                        <Avatar size={21} style={{ marginRight: 8 }} src="/images/avatar.png" alt="Avatar" />
                        <Col flex="auto">
                            <Row>
                                <Col flex="auto">
                                    <Typography size="body1" style={{ fontWeight: 900 }}>
                                        {user?.username}
                                    </Typography>
                                </Col>
                                {user?.isAdmin && (
                                    <Col>
                                        <Tag className="admin-label">ADMIN</Tag>
                                    </Col>
                                )}
                            </Row>
                            <Row style={{ paddingBottom: 8, borderBottom: '1px solid #DDE7EA' }}>
                                <Typography size="body2" className="tile-blue-color">
                                    {description}
                                </Typography>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Col>
    );
};

export default GuildUser;

import React from 'react';
import './Header.scss';
import { Row, Col, Layout, Dropdown, Menu, Divider } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { SkinOutlined, BugOutlined, UserOutlined, LogoutOutlined } from '@ant-design/icons';
import Typography from 'Elements/Typography/Typography';
import PinapleIcon from 'Elements/Icons/PinapleIcon';
import PlayerAvatar from 'Elements/Icons/PlayerAvatar';
import { logoutUser } from 'api/users-service';

const Header = () => {
    return (
        <Layout.Header className="Header">
            <div className="Header-container">
                <Row align="middle" style={{ width: '100%' }}>
                    <Col flex="auto">
                        <Row style={{ height: 'inherit' }} align="middle" className="Header-row" gutter={[20, 0]}>
                            <Col>
                                <div className="Header-logo">
                                    <Link to="/" style={{ display: 'flex', alignItems: 'center' }}>
                                        <PinapleIcon />
                                    </Link>
                                </div>
                            </Col>

                            <Col>
                                <Link to="/all-bugs">
                                    <Typography size="body1" style={{ fontWeight: 400, color: 'white' }}>
                                        <BugOutlined style={{ fontSize: 14, margin: '0 5px -1px 0' }} /> Все баги
                                    </Typography>
                                </Link>
                            </Col>
                            <Col>
                                <Link to="/shop">
                                    <Typography size="body1" style={{ fontWeight: 400, color: 'white' }}>
                                        <SkinOutlined style={{ fontSize: 14, margin: '0 5px -1px 0' }} /> Магазин наград
                                    </Typography>
                                </Link>
                            </Col>
                        </Row>
                    </Col>
                    <Col>
                        <Row>
                            <Link to="/add-new-bug" className="Header-plus">
                                <img src="icons/Plus.svg" alt="Add new bug" className="Header-plus" />
                            </Link>
                            <Dropdown overlay={<ProfileDropdown />} placement="bottomRight">
                                <div className="Header-profile">
                                    <PlayerAvatar />
                                </div>
                            </Dropdown>
                        </Row>
                    </Col>
                </Row>
            </div>
        </Layout.Header>
    );
};

const ProfileDropdown = () => {
    const history = useHistory();

    const logout = () => {
        logoutUser();
        window.location.reload();
    };

    return (
        <Menu>
            <Menu.Item style={{ display: 'flex', alignItems: 'center' }} onClick={() => history.push('/')}>
                <Typography size="body1" style={{ fontWeight: 400 }} className="dark-blue-text">
                    <UserOutlined style={{ fontSize: 14, margin: '0 5px -1px 0' }} />
                    Профиль
                </Typography>
            </Menu.Item>
            <Divider style={{ borderTop: '1px solid rgba(83, 134, 151, 0.2)' }} />
            <Menu.Item danger style={{ display: 'flex', alignItems: 'center' }} onClick={() => logout()}>
                <Typography size="body1" style={{ fontWeight: 400 }}>
                    <LogoutOutlined style={{ fontSize: 14, margin: '0 5px -1px 0' }} />
                    Выйти
                </Typography>
            </Menu.Item>
        </Menu>
    );
};

export default Header;

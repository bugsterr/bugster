import React from 'react';

import './TabView.scss';

import BugItem from 'Elements/BugItem/BugItem';
import Typography from 'Elements/Typography/Typography';

import { useData } from 'api/use-data';
import { BugsService } from 'api/bugs-service';
import { Row, Col, Skeleton } from 'antd';
import times from 'lodash/times';

export interface TabsItems {
    title?: string;
    tabType?: string;
    status?: 'reviewing' | 'rejected' | 'accepted';
    testedApp?: string;
    span?: number;
}

export const TabView = ({ title, status, testedApp, tabType, span }: TabsItems) => {
    const data = useData(BugsService.getArray, { type: tabType, status: status, testedApp: testedApp });

    const spanWidth = span || 24;

    return (
        <React.Fragment>
            {title ? (
                <Typography size="h3" bottom={18}>
                    {title}
                </Typography>
            ) : null}
            {data.map({
                loading: () => (
                    <Row gutter={[0, 16]}>
                        {times(8, (i) => (
                            <Col span={spanWidth} key={i}>
                                <Skeleton.Button active style={{ width: '100%', height: 110 }} />
                            </Col>
                        ))}
                    </Row>
                ),
                error: (error) => null,
                success: (bugs) => {
                    return (
                        <Row gutter={[0, 16]}>
                            {bugs.length > 0 ? (
                                bugs.map((bug, index) => {
                                    return <BugItem key={index} bug={bug} span={spanWidth} />;
                                })
                            ) : (
                                <div>Пока никто не нашел таких багов 😔</div>
                            )}
                        </Row>
                    );
                },
            })}
        </React.Fragment>
    );
};

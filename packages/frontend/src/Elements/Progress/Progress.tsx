import React from 'react';
import { Row, Col, Space, Progress as ProgressBar } from 'antd';
import Typography from 'Elements/Typography/Typography';

export interface ProgressProps {
    title: string;
    level: number;
    experience: number;
    total_experience: number;
    gradient: 'red' | 'pink' | 'blue';
}

const Progress: React.FunctionComponent<ProgressProps> = ({
    title,
    level,
    experience,
    total_experience,
    gradient,
}: ProgressProps) => {
    const gradientMap = {
        blue: { from: '#1bd2fa', to: '#1053ff' },
        pink: { from: '#ba26f1', to: '#e051af' },
        red: { from: '#ff9f1c', to: '#ff3e01' },
    };

    return (
        <Row>
            <Col span={24}>
                <Row>
                    <Col flex="auto">
                        <Typography size="body1" className="dark-blue-color">
                            {title}
                        </Typography>
                    </Col>
                    <Col>
                        <Space>
                            <Typography size="body2" className="tile-blue-color">
                                {level} LVL
                            </Typography>
                            <Typography size="body1" className="dark-blue-color">
                                •
                            </Typography>
                            <Typography size="body1" className="dark-blue-color">
                                {experience}/{total_experience} XP
                            </Typography>
                        </Space>
                    </Col>
                </Row>
            </Col>
            <Col span={24}>
                <ProgressBar
                    trailColor="#eeeeee"
                    strokeColor={gradientMap[gradient]}
                    percent={(experience / total_experience) * 100}
                    showInfo={false}
                />
            </Col>
        </Row>
    );
};

export default Progress;

import React from 'react';

import './Form.scss';

const Form: React.FunctionComponent<React.FormHTMLAttributes<HTMLFormElement>> = ({ children, ...props }) => {
    return (
        <form className="Form" {...props}>
            {children}
        </form>
    );
};

export default Form;

import React from 'react';
import classNames from 'classnames';

import { Input as AntdInput } from 'antd';

import './Input.scss';

export type InputProps = React.ComponentProps<typeof AntdInput>;

const Input: React.FunctionComponent<InputProps> = ({ className, ...props }) => {
    return <AntdInput className={classNames('Input', className)} {...props} />;
};

export default Input;

import { withField } from 'withField';

import { Select as AntdSelect } from 'antd';

import Form from './Form/Form';
import Input from './Input/Input';
import TextArea from './TextArea/TextArea';
import Select from './Select/Select';
import Checkbox from './Checkbox/Checkbox';
import Selector from './Selector/Selector';

export { default as Error } from './Error/Error';

export { Form as FormComponent, Input, TextArea, Select, Selector };

const FieldInput = withField<string | undefined>()(Input);
const FieldTextArea = withField<string | undefined>()(TextArea);
const FieldSelect = withField<number | undefined>()(Select);
const FieldSelector = withField<string | undefined>()(Selector);
const FieldCheckbox = withField<boolean>()(Checkbox);

const { Option: SelectOption } = AntdSelect;

export { FieldInput, FieldTextArea, FieldSelect, FieldSelector, SelectOption, FieldCheckbox };

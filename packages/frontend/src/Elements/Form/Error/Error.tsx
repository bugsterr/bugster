import React from 'react';
import { Field } from 'react-final-form';

import { Alert } from 'antd';

export interface ErrorProps {
    fieldName: string;
    className?: string;
}

const Error: React.FunctionComponent<ErrorProps> = ({ fieldName, className }) => {
    return (
        <Field name={fieldName}>
            {({ meta }) => {
                return meta.touched && meta.error ? (
                    <Alert className={className} message={meta.error} type="error" />
                ) : null;
            }}
        </Field>
    );
};

export default Error;

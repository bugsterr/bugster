import React from 'react';
import classNames from 'classnames';

import { Input } from 'antd';

import './TextArea.scss';

const { TextArea: AntdTextArea } = Input;

export type InputProps = React.ComponentProps<typeof AntdTextArea>;

const TextArea: React.FunctionComponent<InputProps> = ({ className, ...props }) => {
    return <AntdTextArea className={classNames('TextArea', className)} {...props} />;
};

export default TextArea;

import React from 'react';
import { Radio, Space } from 'antd';
import './Selector.scss';
import classNames from 'classnames';
import { RadioChangeEvent } from 'antd/lib/radio';

export interface Data {
    value: string;
    title: string;
}

interface SelectorProps {
    data: Array<Data>;
    label: string;
    buttonStyle: 'outline' | 'solid';
    value?: any;
    onChange?: (e: RadioChangeEvent) => void;
    className?: string;
}

const Selector = ({ label, data, className, buttonStyle, value, onChange }: SelectorProps) => {
    return (
        <div className="Selector">
            <span>{label}:</span>
            <Radio.Group buttonStyle={buttonStyle} className={className} value={value} onChange={onChange}>
                <Space>
                    {data.map((item) => {
                        return (
                            <Radio.Button
                                value={item.value}
                                key={item.value}
                                className={classNames('radio-selector', item.value)}
                            >
                                {item.title}
                            </Radio.Button>
                        );
                    })}
                </Space>
            </Radio.Group>
        </div>
    );
};

export default Selector;

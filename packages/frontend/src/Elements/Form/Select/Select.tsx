import React from 'react';
import classNames from 'classnames';

import { Select as AntdSelect } from 'antd';

import './Select.scss';

export type SelectProps = React.ComponentProps<typeof AntdSelect>;

const Select: React.FunctionComponent<SelectProps> = ({ className, value, ...props }) => {
    return <AntdSelect className={classNames('Select', className)} value={value || undefined} {...props} />;
};

export default Select;

import React from 'react';
import classNames from 'classnames';

import { Checkbox as AntdCheckbox } from 'antd';

import './Checkbox.scss';

export type InputProps = React.ComponentProps<typeof AntdCheckbox>;

const Checkbox: React.FunctionComponent<InputProps> = ({ className, ...props }) => {
    return <AntdCheckbox className={classNames('Checkbox', className)} {...props} />;
};

export default Checkbox;

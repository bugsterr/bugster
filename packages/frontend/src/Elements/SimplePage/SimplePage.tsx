import React from 'react';

import { Row, Divider } from 'antd';

import Typography from 'Elements/Typography/Typography';

import './SimplePage.scss';

export interface SimplePageProps {
    title: string;
    right?: React.ReactNode;
}

const SimplePage: React.FunctionComponent<SimplePageProps> = ({ title, right, children }) => {
    return (
        <div className="SimplePage">
            <Row style={{ height: 70 }} align="middle">
                <Typography size="h1">{title}</Typography>
                <div className="SimplePage-right">{right}</div>
            </Row>
            <Divider className="mb-20" />
            {children}
        </div>
    );
};

export default SimplePage;

import React from 'react';

import { Button, Col, Row } from 'antd';
import { GuildsService } from 'api/guilds-service';
import Typography from 'Elements/Typography/Typography';
import { useUsers } from 'Users';

export const GuildItem = ({ guild }: { guild: any }) => {
    const { getUser } = useUsers();
    const join = () => {
        GuildsService.join({ guildId: guild._id }).then(() => {
            window.location.reload();
        });
    };

    return (
        <React.Fragment>
            <div className="Guild-item">
                <Row gutter={[8, 8]}>
                    <Col span={12}>
                        <Typography size="h3" className="Guild-title">
                            {guild.name}
                        </Typography>
                        <Typography size="body2">Создатель: {getUser(guild.creator)?.username}</Typography>
                        <Typography size="body2">{guild.members.length} участников</Typography>
                    </Col>
                    <Col span={12}>
                        <Button type="primary" className="Guild-login" onClick={join}>
                            ВСТУПИТЬ
                        </Button>
                    </Col>
                </Row>
            </div>
        </React.Fragment>
    );
};

import React from 'react';

import Typography from 'Elements/Typography/Typography';

import './SlimPage.scss';

export interface SlimPageProps {
    title: string;
    right?: React.ReactNode;
}

const SlimPage: React.FunctionComponent<SlimPageProps> = ({ title, right, children }) => {
    return (
        <div className="SlimPage">
            <div className="SlimPage-subheader">
                <div className="SlimPage-subheader-inner">
                    <Typography size="h1">{title}</Typography>
                    {right}
                </div>
            </div>
            <div className="SlimPage-main">{children}</div>
        </div>
    );
};

export default SlimPage;

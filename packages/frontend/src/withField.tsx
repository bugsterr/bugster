import React from 'react';

import { Field, FieldInputProps, UseFieldConfig } from 'react-final-form';

export interface FieldProps<U> extends UseFieldConfig<U> {
    name: string;
}

const FieldPropsKeys = [
    'name',
    'afterSubmit',
    'allowNull',
    'beforeSubmit',
    'defaultValue',
    'format',
    'formatOnBlur',
    'initialValue',
    'isEqual',
    'multiple',
    'parse',
    'subscription',
    'type',
    'validate',
    'validateFields',
    'value',
];

type AnyObject = {
    [key: string]: any;
};

const OmitKeys = <T,>(target: AnyObject, keys: string[], opposite = false) => {
    const result: AnyObject = {};
    for (const key of Object.keys(target)) {
        if (keys.includes(key) !== opposite) {
            result[key] = target[key];
        }
    }
    return result as T;
};

type UserInputKeys = 'name' | 'type' | 'value' | 'checked' | 'multiple' | 'onChange';

type UsedInput = Pick<FieldInputProps<any>, UserInputKeys>;

export function withField<U>() {
    return function <T>(WrappedComponent: React.ComponentType<T>) {
        // Try to create a nice displayName for React Dev Tools.
        const displayName = WrappedComponent.displayName || WrappedComponent.name || 'Component';

        type Props = Omit<T, keyof UsedInput> & FieldProps<U>;

        // Creating the inner component. The calculated Props type here is the where the magic happens.
        return class ComponentWithField extends React.Component<Props> {
            public static displayName = `withField(${displayName})`;

            public render() {
                const fieldProps = OmitKeys<FieldProps<U>>(this.props, FieldPropsKeys);
                const componentProps = OmitKeys<T>(this.props, FieldPropsKeys, true);
                return (
                    <Field<U> {...fieldProps}>
                        {({ input, meta }) => (
                            <>
                                <WrappedComponent {...componentProps} {...(input as UsedInput)} />
                            </>
                        )}
                    </Field>
                );
            }
        };
    };
}

import React from 'react';
import './App.scss';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Layout } from 'antd';
import Header from './Elements/Header/Header';
import AddNewBug from 'Pages/AddNewBug/AddNewBug';
import Profile from 'Pages/Profile/Profile';
import AllBugsPage from 'Pages/AllBugsPage/AllBugsPage';
import Shop from 'Pages/Shop/Shop';
import ProtectedRoutes from './ProtectedRoutes';

function App() {
    const { Content } = Layout;

    return (
        <Layout>
            <ProtectedRoutes>
                <Header />
                <Content>
                    <Switch>
                        <Route exact path="/" component={Profile} />
                        <Route exact path="/add-new-bug" component={AddNewBug} />
                        <Route exact path="/all-bugs" component={AllBugsPage} />
                        <Route exact path="/shop" component={Shop} />
                        <Route path="*" render={() => <Redirect to="/" />} />
                    </Switch>
                </Content>
            </ProtectedRoutes>
        </Layout>
    );
}

export default App;

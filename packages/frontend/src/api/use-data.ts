import { useCallback, useMemo, useState } from 'react';

import useDeepCompareEffect from './use-deep-compare-effect';

type Arr = readonly unknown[];

type Mappings<T, U> = {
    loading: () => U;
    success: (data: T) => U;
    error: (error: string) => U;
};

type Mapper<T> = <U>(mappings: Mappings<T, U>) => U;

type Data<T> =
    | {
          type: 'loading';
      }
    | {
          type: 'success';
          data: T;
      }
    | {
          type: 'error';
          error: string;
      };

export function useData<R extends Arr, T>(f: (...args: R) => Promise<T>, ...args: R): Data<T> & { map: Mapper<T> } {
    const [data, setData] = useState<Data<T>>({ type: 'loading' });

    const map: (data: Data<T>) => Mapper<T> = useCallback(
        (data: Data<T>) => <U>(mappings: Mappings<T, U>) => {
            switch (data.type) {
                case 'loading':
                    return mappings.loading();
                case 'success':
                    return mappings.success(data.data);
                case 'error':
                    return mappings.error(data.error);
            }
        },
        [],
    );

    useDeepCompareEffect(() => {
        let didCancel = false;

        const getData = async () => {
            try {
                setData({
                    type: 'loading',
                });
                const t = await f(...args);
                !didCancel &&
                    setData({
                        type: 'success',
                        data: t,
                    });
            } catch (error) {
                !didCancel &&
                    setData({
                        type: 'error',
                        error: error.toString(),
                    });
            }
        };
        getData();
        return () => {
            didCancel = true;
        };
    }, args);

    const result = useMemo(() => {
        return {
            ...data,
            map: map(data),
        };
    }, [data, map]);

    return result;
}

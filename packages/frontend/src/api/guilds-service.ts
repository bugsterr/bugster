import { Bug, Guild } from 'common';
import api from './api';

export const GuildsService = {
    create: (request: { guildName: string }) => api('/guilds/create', request) as Promise<{ url: string }>,
    join: (request: { guildId: string }) => api('/guilds/join', request) as Promise<{ url: string }>,
    getList: () => api('/guilds/list') as Promise<Array<Guild>>,
    getMyGuild: (request: { userId: string }) =>
        api('/guilds/my-guild', request) as Promise<{ guild: Guild; bugs: Bug[] }>,
};

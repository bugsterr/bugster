import { User, Experience } from 'common';
import api from './api';
import cookie from 'react-cookies';

export interface UserRegister {
    username: string;
    fullName: string;
}

export const UsersService = {
    register: (request: UserRegister) => {
        return api('/users/register', request) as Promise<{ token: string }>;
    },
    login: (request: { username: string }) => {
        return api('/users/login', request) as Promise<{ success: boolean; token: string }>;
    },
    get: (request: { id: string }) => {
        return api('/users/get', request) as Promise<User>;
    },
    getMe: () => {
        return api('/users/me') as Promise<User>;
    },
    getExp: (request: { userId: string; type: string }) => {
        return api('/users/get-exp', request) as Promise<Experience>;
    },
};

export const loginUser = (username: string, remember: boolean) =>
    UsersService.login({ username }).then(({ token }) => {
        const options = remember ? { maxAge: 2592000, path: '/' } : { path: '/' };
        cookie.remove('jwt-key'); // can be old jwt-key without options
        cookie.save('jwt-key', token, options);
    });

export const registerUser = (values: UserRegister, remember: boolean) =>
    UsersService.register(values).then(({ token }) => {
        const options = remember ? { maxAge: 2592000, path: '/' } : { path: '/' };
        cookie.remove('jwt-key'); // can be old jwt-key without options
        cookie.save('jwt-key', token, options);
    });

export const logoutUser = () => {
    cookie.remove('jwt-key', { path: '/' });
};

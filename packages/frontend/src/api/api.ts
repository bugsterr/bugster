import cookie from 'react-cookies';

export const baseUrl = 'багстер.рф/api';
// export const serverUrl = baseUrl + '/api';
export const secure = true;

export function status(response: Response) {
    if ((response.status >= 200 && response.status < 300) || response.status === 500) {
        return Promise.resolve(response);
    } else {
        return Promise.reject(new Error(response.statusText));
    }
}

export function json(response: Response) {
    return response.json();
}

export function throwErr(response: any) {
    if (response.error) {
        return Promise.reject(new Error(response.error));
    } else {
        return Promise.resolve(response);
    }
}

export default function api(url: string, body?: any, headers?: Record<string, string>, mainUrl = baseUrl) {
    console.log({ url, headers, body });

    const token = cookie.load('jwt-key');
    const finalHeaders: Record<string, string> = {
        ...(token && { Authorization: `Bearer ${token}` }),
        'Content-Type': 'application/json',
        Accept: 'application/json',
        ...headers,
    };

    const securityType = secure ? 'https://' : 'http://';
    const buildedUrl = `${securityType}${mainUrl}${url}`;

    return fetch(buildedUrl, {
        method: 'POST',
        headers: finalHeaders,
        body: JSON.stringify(body) || '{}',
    })
        .then(status)
        .then(json)
        .then(throwErr);
}

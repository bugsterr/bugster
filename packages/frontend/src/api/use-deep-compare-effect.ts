import React from 'react';
import _ from 'lodash';

type Arr = readonly unknown[];

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function checkDeps<T extends Arr>(deps: T) {
    if (!deps || !deps.length) {
        throw new Error('useDeepCompareEffect should not be used with no dependencies. Use React.useEffect instead.');
    }
    if (deps.every((d) => !_.isObject(d))) {
        throw new Error(
            'useDeepCompareEffect should not be used with dependencies that are all primitive values. Use React.useEffect instead.',
        );
    }
}

function useDeepCompareMemoize<T>(value: T) {
    const ref = React.useRef<T>();

    if (!_.isEqual(value, ref.current)) {
        ref.current = value;
    }

    return ref.current;
}

function useDeepCompareEffect<T extends Arr>(callback: () => void, dependencies: T): void {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    React.useEffect(callback, useDeepCompareMemoize(dependencies));
}

export default useDeepCompareEffect;

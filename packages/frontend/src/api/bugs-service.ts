import { Attachment, Bug, Comment, Template } from 'common';
import api from './api';
import { UsersService } from './users-service';

export interface BugCreate {
    title: string;
    description: string;
    type: string;
    testedApp: string;
    appVersion: string;
    deviceInfo: string;
    attachments: Buffer[];
    // Save bug template for further usage
    template?: boolean;
}

export interface BugFind {
    creator?: string; // creator_id
    type?: string;
    status?: 'reviewing' | 'rejected' | 'accepted';
    testedApp?: string;
    amount?: number;
    offset?: number;
}

export const BugsService = {
    new: (request: BugCreate) => api('/bugs/create', request) as Promise<{ success: boolean }>,
    get: (request: BugFind) => api('/bugs/find', request) as Promise<{ bug: Bug }>,
    getArray: (request: BugFind) => api('/bugs/find', request) as Promise<Bug[]>,
    getTemplates: (request: { creator: string }) => api('/bugs/get-templates', request) as Promise<Template[]>,
    getAttachment: (request: { relatedBug: string }) => api('/bugs/get-attachment', request) as Promise<Attachment>,
    comment: (request: { text: string; relatedBug: string }) => api('/bugs/comment', request) as Promise<Comment>,
    getComments: (request: { relatedBug: string }) => api('/bugs/get-comments', request) as Promise<Comment[]>,
    resolve: (request: { id: string; status: boolean }) => api('/bugs/resolve', request) as Promise<Bug>,
};

export const getMyBugs = async ({ amount }: { amount?: number }) => {
    const myUser = await UsersService.getMe();
    if (myUser) {
        return await BugsService.getArray({ creator: myUser._id, amount });
    }
};

export const getMyTemplates = async () => {
    const myUser = await UsersService.getMe();
    if (myUser) {
        return await BugsService.getTemplates({ creator: myUser._id });
    }
};

export const getBug = (values: BugFind) => {
    return BugsService.get(values).then((bug) => {
        return bug;
    });
};

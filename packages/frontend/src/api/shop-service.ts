import api from './api';
import { Product, Order } from 'common';

function notUndefined(entry: readonly [Order, Product | undefined]): entry is [Order, Product] {
    return entry[1] !== undefined;
}

export const ShopService = {
    getProducts: () => api('/shop/get-products') as Promise<Product[]>,
    getOrders: () => api('/shop/get-orders') as Promise<Order[]>,
    buy: (request: { product: string }) => api('/shop/buy', request) as Promise<Order>,
    getShopData: async () => {
        const products = await ShopService.getProducts();
        const orders = await ShopService.getOrders();
        const entries = orders
            .map((order) => {
                const product = products.find((p) => p._id === order.product);
                return [order, product] as const;
            })
            .filter(notUndefined);
        return {
            products,
            entries,
        };
    },
};

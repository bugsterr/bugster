import { useCallback } from 'react';

export type Dispatch<T> = (value: T) => void;
export type Get<T> = () => T | undefined;

function useLocalStorage<T>(name: string) {
    const getState: Get<T> = useCallback(() => {
        const s = localStorage.getItem(name);
        return s ? (JSON.parse(s) as T) : undefined;
    }, [name]);

    const setState: Dispatch<T | undefined> = useCallback(
        (value: T | undefined) => {
            const s = value ? JSON.stringify(value) : undefined;
            if (s) {
                localStorage.setItem(name, s);
            } else {
                localStorage.removeItem(name);
            }
        },
        [name],
    );

    return [getState, setState] as const;
}

export default useLocalStorage;

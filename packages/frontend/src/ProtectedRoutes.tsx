import React from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import AuthourizePage from './Pages/AuthourizePage/AuthourizePage';
import cookie from 'react-cookies';

const ProtectedRoutes: React.FC = ({ children }) => {
    const logged = cookie.load('jwt-key');
    return (
        <Router>
            {logged ? (
                children
            ) : (
                <Switch>
                    <Route exact path="/login" component={AuthourizePage} />
                    <Route path="*" render={() => <Redirect to="/login" />} />
                </Switch>
            )}
        </Router>
    );
};

export default ProtectedRoutes;

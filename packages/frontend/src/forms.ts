export function required<T>(value?: T) {
    if (value === undefined || value === null) return 'Обязательное поле';
    if (typeof value === 'string' && value === '') return 'Обязательное поле';
    return undefined;
}

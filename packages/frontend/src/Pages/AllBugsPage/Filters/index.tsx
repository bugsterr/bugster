import React from 'react';
import { Row, Col } from 'antd';
import { Select, SelectOption } from 'Elements/Form';
import { SelectValue } from 'antd/lib/select';

interface FiltersProps {
    OnStatusChange?: ((status: string | undefined) => void) | undefined;
    OnTestedAppChange?: ((testedApp: string | undefined) => void) | undefined;
}

const Filters: React.FunctionComponent<FiltersProps> = ({ OnStatusChange, OnTestedAppChange }: FiltersProps) => {
    const processVal = (val: SelectValue) => {
        const str = val.toString();
        return str === 'any' ? undefined : str;
    };

    return (
        <Row gutter={[8, 8]}>
            <Col span={12}>
                <Select
                    onChange={(val) => OnStatusChange && OnStatusChange(processVal(val))}
                    className="AddNewBug-select"
                    placeholder="Статус"
                >
                    <SelectOption value="any">Любой</SelectOption>
                    <SelectOption value="reviewing">На проверке</SelectOption>
                    <SelectOption value="accepted">Принято</SelectOption>
                    <SelectOption value="rejected">Отклонено</SelectOption>
                </Select>
            </Col>
            <Col span={12}>
                <Select
                    onChange={(val) => OnTestedAppChange && OnTestedAppChange(processVal(val))}
                    className="AddNewBug-select"
                    placeholder="Приложение"
                >
                    <SelectOption value="any">Любое</SelectOption>
                    <SelectOption value="Мобильный банк">Мобильный банк</SelectOption>
                    <SelectOption value="Веб-сайт">Веб-сайт</SelectOption>
                    <SelectOption value="Онлайн-банк">Онлайн-банк</SelectOption>
                </Select>
            </Col>
        </Row>
    );
};

export default Filters;

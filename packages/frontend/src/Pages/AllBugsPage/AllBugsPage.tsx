import React, { useState } from 'react';
import './AllBugsPage.scss';
import Page from 'Elements/Page/Page';
import TabPane from 'Elements/TabPane/TabPane';

import { BookOutlined, ReadOutlined, ThunderboltOutlined, PartitionOutlined } from '@ant-design/icons';
import { TabView } from 'Elements/TabsView/TabView';

import Filters from './Filters';

const AllBugsPage = () => {
    const [status, setStatus] = useState<string>();
    const [testedApp, setTestedApp] = useState<string>();

    return (
        <Page
            title="Баги"
            tabs={[
                <TabPane icon={<ReadOutlined />} key="all-bugs">
                    Все баги
                </TabPane>,
                <TabPane icon={<PartitionOutlined />} key="functional">
                    Функционал
                </TabPane>,
                <TabPane icon={<BookOutlined />} key="design">
                    Дизайн
                </TabPane>,
                <TabPane icon={<ThunderboltOutlined />} key="vulnerability">
                    Уязвимость
                </TabPane>,
            ]}
            sidebar={<Filters OnStatusChange={setStatus} OnTestedAppChange={setTestedApp} />}
            main={[
                {
                    key: 'all-bugs',
                    render: (
                        <TabView
                            testedApp={testedApp}
                            status={status as 'reviewing' | 'rejected' | 'accepted' | undefined}
                        />
                    ),
                },
                {
                    key: 'functional',
                    render: (
                        <TabView
                            testedApp={testedApp}
                            status={status as 'reviewing' | 'rejected' | 'accepted' | undefined}
                            tabType="functional"
                        />
                    ),
                },
                {
                    key: 'design',
                    render: (
                        <TabView
                            testedApp={testedApp}
                            status={status as 'reviewing' | 'rejected' | 'accepted' | undefined}
                            tabType="design"
                        />
                    ),
                },
                {
                    key: 'vulnerability',
                    render: (
                        <TabView
                            testedApp={testedApp}
                            status={status as 'reviewing' | 'rejected' | 'accepted' | undefined}
                            tabType="vulnerability"
                        />
                    ),
                },
            ]}
        />
    );
};

export default AllBugsPage;

import React, { useState } from 'react';

import { useHistory } from 'react-router-dom';
import { FORM_ERROR } from 'final-form';
import { Field, Form } from 'react-final-form';
import { OnChange } from 'react-final-form-listeners';

import './AddNewBug.scss';

import {
    FormComponent,
    FieldInput as Input,
    FieldTextArea as TextArea,
    FieldSelect as Select,
    FieldSelector as Selector,
    SelectOption,
    FieldCheckbox,
    Error,
} from 'Elements/Form';
import Typography from 'Elements/Typography/Typography';
import { Row, Col, Button, Alert, Upload } from 'antd';
import { BugsService, getMyTemplates } from 'api/bugs-service';
import { Template as TemplateType } from 'common';
import { required } from 'forms';

import { FileImageOutlined, UndoOutlined } from '@ant-design/icons';

import { RcCustomRequestOptions, UploadChangeParam, UploadFile } from 'antd/lib/upload/interface';
import useLocalStorage from 'use-local-storage';
import SimplePage from 'Elements/SimplePage/SimplePage';

import { bugTypes, deviceInfo1, deviceInfo2, testedApp } from '../../params';
import { useData } from 'api/use-data';

function notUndefined<T>(x: T | undefined): x is T {
    return x !== undefined;
}

const getBase64 = (file?: File | Blob): Promise<string | ArrayBuffer | null> => {
    return new Promise((resolve, reject) => {
        if (!file) reject();
        else {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = (error) => reject(error);
        }
    });
};

type FormValues = {
    title: string;
    description: string;
    type: string;
    testedApp: string;
    appVersion: string;
    deviceInfo1: string;
    deviceInfo2: string;
    save_template: boolean;
};

interface FormTemplate extends TemplateType {
    deviceInfo1?: string;
    deviceInfo2?: string;
    deviceInfo3?: string;
}

const AddNewBug: React.FunctionComponent = () => {
    const [files, setFiles] = useState<UploadFile<any>[]>([]);
    const history = useHistory();
    const [getPrev, setPrev] = useLocalStorage<FormValues>('prev-bug');
    const [initialValues, setInitialValues] = useState<Partial<FormValues>>();

    const data = useData(getMyTemplates);

    const templates: Record<string, FormTemplate[]> | undefined = data.map({
        loading: () => undefined,
        error: () => undefined,
        success: (tmps) => {
            let res: Record<string, FormTemplate[]> = {};
            tmps &&
                tmps.forEach((tmp) => {
                    const testedApp = tmp.testedApp as string;

                    // I'm sorry, this is nasty.
                    // It's just too late to rework db schema
                    let tmpCopy: FormTemplate = tmp;
                    tmpCopy.deviceInfo1 = tmp.deviceInfo.split(';')[0];
                    tmpCopy.deviceInfo2 = tmp.deviceInfo.split(';')[1];

                    if (res[testedApp]) {
                        res[testedApp].push(tmpCopy);
                    } else {
                        res[testedApp] = [tmpCopy];
                    }
                });
            return res;
        },
    });

    const onSubmit = async (values: FormValues) => {
        try {
            const filesToBuffer: Buffer[] = (
                await Promise.all(
                    files.map(async (file: UploadFile<any>) => {
                        const base64 = await getBase64(file.originFileObj);
                        return base64 ? Buffer.from(base64) : undefined;
                    }),
                )
            ).filter(notUndefined);

            setPrev(values);

            await BugsService.new({
                title: values.title,
                description: values.description,
                type: values.type || 'design',
                testedApp: values.testedApp,
                appVersion: values.appVersion,
                deviceInfo: values.deviceInfo1 + ';' + values.deviceInfo2,
                attachments: filesToBuffer,
                template: values.save_template,
            });

            history.push('/');
        } catch (error) {
            return {
                [FORM_ERROR]: '' + error,
            };
        }
    };

    const customRequest = ({ file, onSuccess }: RcCustomRequestOptions) => {
        setTimeout(() => {
            onSuccess({}, file);
        }, 0);
    };

    const onUpload = ({ fileList }: UploadChangeParam) => {
        setFiles(fileList);
    };

    return (
        <SimplePage
            title="Новый баг"
            right={
                <Button type="default" icon={<UndoOutlined />} onClick={() => setInitialValues(getPrev())}>
                    Как прошлый баг
                </Button>
            }
        >
            <Row align="top">
                <Col span={24}>
                    <Form
                        mutators={{
                            resetField: ([field], state, { changeValue, resetFieldState }) => {
                                changeValue(state, field, () => undefined);
                                resetFieldState(field);
                            },
                        }}
                        onSubmit={onSubmit}
                        subscription={{ submitError: true, submitting: true }}
                        initialValues={initialValues}
                        render={({ form, handleSubmit, submitError, submitting }) => (
                            <FormComponent onSubmit={handleSubmit}>
                                <Row>
                                    <Col span={10} style={{ paddingRight: 20 }}>
                                        <Typography size="h4" className="mt-16 mb-16">
                                            Мои шаблоны
                                        </Typography>
                                        {templates ? (
                                            <>
                                                {Object.keys(templates).map((item: any) => (
                                                    <div key={item}>
                                                        <Typography size="body1" style={{ fontWeight: 900 }}>
                                                            {testedApp[item]}
                                                        </Typography>
                                                        <Row gutter={[0, 20]} style={{ marginTop: 20 }}>
                                                            {(templates as any)[item].map((data: any) => (
                                                                <Col
                                                                    span={24}
                                                                    key={data.id}
                                                                    className="templateItem"
                                                                    onClick={() => {
                                                                        setInitialValues(() => {
                                                                            setInitialValues(data);
                                                                            return {};
                                                                        });
                                                                    }}
                                                                >
                                                                    <div className="Params">
                                                                        <div className="Params-left">
                                                                            <Typography size="body2">
                                                                                Версия приложения
                                                                            </Typography>
                                                                            <Typography size="body2">
                                                                                {deviceInfo1[item].placeholder}
                                                                            </Typography>
                                                                            <Typography size="body2">
                                                                                {deviceInfo2[item].placeholder}
                                                                            </Typography>
                                                                        </div>
                                                                        <div className="Params-right">
                                                                            <Typography size="body2">
                                                                                {data.appVersion}
                                                                            </Typography>
                                                                            <Typography size="body2">
                                                                                {
                                                                                    deviceInfo1[item].options.find(
                                                                                        (o) =>
                                                                                            o.value ===
                                                                                            data.deviceInfo1,
                                                                                    )?.label
                                                                                }
                                                                            </Typography>
                                                                            <Typography size="body2">
                                                                                {(() => {
                                                                                    const params = deviceInfo2[item];

                                                                                    if (params.type === 'dropdown') {
                                                                                        return params.options[
                                                                                            data.deviceInfo1
                                                                                        ]?.find(
                                                                                            (o) =>
                                                                                                o.value ===
                                                                                                data.deviceInfo2,
                                                                                        )?.label;
                                                                                    } else {
                                                                                        return data.deviceInfo2;
                                                                                    }
                                                                                })()}
                                                                            </Typography>
                                                                        </div>
                                                                    </div>
                                                                </Col>
                                                            ))}
                                                        </Row>
                                                    </div>
                                                ))}
                                            </>
                                        ) : (
                                            <Typography size="body2" style={{ color: '#90ACB5', fontWeight: 400 }}>
                                                Здесь будут сохраняться шаблоны настроек бага. Чтобы сохранить настройки
                                                текущего бага, поставьте галочку в чекбоксе «Сохранить текущие
                                                настройки, как шаблон»
                                            </Typography>
                                        )}
                                    </Col>
                                    <Col span={14}>
                                        <Typography size="h4" className="mt-16 mb-16">
                                            Описание бага
                                        </Typography>
                                        <Input name="title" placeholder="Название бага" validate={required} />
                                        <Error fieldName="title" className="mt-8" />
                                        <TextArea
                                            name="description"
                                            placeholder="Шаги воспроизведения"
                                            rows={4}
                                            className="mt-16"
                                            validate={required}
                                        />
                                        <Error fieldName="description" className="mt-8" />
                                        <Selector
                                            name="type"
                                            buttonStyle="solid"
                                            type="input"
                                            data={bugTypes}
                                            label="Категория"
                                        />
                                        <Typography size="h4" className="mt-20 mb-16">
                                            Скриншоты
                                        </Typography>
                                        <Row>
                                            <Col span={24}>
                                                <Upload.Dragger
                                                    name="files"
                                                    multiple
                                                    onChange={onUpload}
                                                    customRequest={customRequest}
                                                >
                                                    <p className="ant-upload-drag-icon">
                                                        <FileImageOutlined />
                                                    </p>
                                                    <Typography size="h4">Кликните или перетащите файлы</Typography>
                                                    <Typography size="body2">
                                                        Ваша персональная иформация не будет показана сообществу
                                                    </Typography>
                                                </Upload.Dragger>
                                            </Col>
                                        </Row>
                                        <Typography size="h4" className="mt-16 mb-16">
                                            Параметры приложения
                                        </Typography>
                                        <Row gutter={[8, 8]}>
                                            <Col span={12}>
                                                <Select
                                                    className="AddNewBug-select"
                                                    name="testedApp"
                                                    placeholder="Тестируемое приложение"
                                                    validate={required}
                                                >
                                                    <SelectOption value="mobile">Мобильный банк</SelectOption>
                                                    <SelectOption value="web">Веб-сайт</SelectOption>
                                                    <SelectOption value="online">Онлайн-банк</SelectOption>
                                                </Select>
                                                <Error fieldName="testedApp" className="mt-8" />
                                            </Col>
                                            <Col span={12}>
                                                <Select
                                                    className="AddNewBug-select"
                                                    name="appVersion"
                                                    placeholder="Версия приложения"
                                                    validate={required}
                                                >
                                                    <SelectOption value="1.0">1.0</SelectOption>
                                                    <SelectOption value="1.1">1.1</SelectOption>
                                                </Select>
                                                <Error fieldName="appVersion" className="mt-8" />
                                            </Col>
                                        </Row>
                                        <Field name="testedApp">
                                            {({ input: { value: testedApp } }) =>
                                                testedApp ? (
                                                    <>
                                                        <Typography size="h4" className="mt-20 mb-16">
                                                            Дополнительное
                                                        </Typography>
                                                        <Row gutter={[8, 8]}>
                                                            <Col span={12}>
                                                                <Select
                                                                    className="AddNewBug-select"
                                                                    name="deviceInfo1"
                                                                    placeholder={deviceInfo1[testedApp].placeholder}
                                                                    validate={required}
                                                                >
                                                                    {deviceInfo1[testedApp].options.map((option) => (
                                                                        <SelectOption
                                                                            value={option.value}
                                                                            key={option.value}
                                                                        >
                                                                            {option.label}
                                                                        </SelectOption>
                                                                    ))}
                                                                </Select>
                                                                <Error fieldName="deviceInfo1" className="mt-8" />
                                                            </Col>
                                                            <Col span={12}>
                                                                {(() => {
                                                                    const params = deviceInfo2[testedApp];

                                                                    if (params.type === 'dropdown') {
                                                                        return (
                                                                            <Field name="deviceInfo1">
                                                                                {({
                                                                                    input: { value: deviceInfo1value },
                                                                                }) => {
                                                                                    if (
                                                                                        !params.options[
                                                                                            deviceInfo1value
                                                                                        ]
                                                                                    )
                                                                                        return null;

                                                                                    return deviceInfo1value ? (
                                                                                        <Select
                                                                                            key={
                                                                                                testedApp +
                                                                                                deviceInfo1value
                                                                                            }
                                                                                            className="AddNewBug-select"
                                                                                            name="deviceInfo2"
                                                                                            placeholder={
                                                                                                params.placeholder
                                                                                            }
                                                                                            validate={required}
                                                                                        >
                                                                                            {params.options[
                                                                                                deviceInfo1value
                                                                                            ].map((option) => (
                                                                                                <SelectOption
                                                                                                    value={option.value}
                                                                                                    key={option.value}
                                                                                                >
                                                                                                    {option.label}
                                                                                                </SelectOption>
                                                                                            ))}
                                                                                        </Select>
                                                                                    ) : null;
                                                                                }}
                                                                            </Field>
                                                                        );
                                                                    } else {
                                                                        return (
                                                                            <Input
                                                                                key={testedApp}
                                                                                name="deviceInfo2"
                                                                                placeholder={params.placeholder}
                                                                                validate={required}
                                                                            />
                                                                        );
                                                                    }
                                                                })()}
                                                                <Error fieldName="deviceInfo2" className="mt-8" />
                                                            </Col>
                                                        </Row>
                                                    </>
                                                ) : null
                                            }
                                        </Field>
                                        <OnChange name="testedApp">
                                            {(value, previous) => {
                                                if (previous !== undefined && previous !== '') {
                                                    form.mutators.resetField('deviceInfo1');
                                                    form.mutators.resetField('deviceInfo2');
                                                }
                                            }}
                                        </OnChange>
                                        <OnChange name="deviceInfo1">
                                            {(value, previous) => {
                                                if (previous !== undefined && previous !== '') {
                                                    form.mutators.resetField('deviceInfo2');
                                                }
                                            }}
                                        </OnChange>
                                        <FieldCheckbox name="save_template" type="checkbox">
                                            Сохранить текущие настройки, как шаблон
                                        </FieldCheckbox>
                                        <div className="AddNewBug-bottom">
                                            {submitError && (
                                                <Alert
                                                    className="AddNewBug-error"
                                                    message={submitError}
                                                    type="error"
                                                    closable
                                                />
                                            )}
                                            <Button
                                                type="primary"
                                                htmlType="submit"
                                                className="AddNewBug-submit"
                                                style={{ marginLeft: 'auto' }}
                                                loading={submitting}
                                            >
                                                СОЗДАТЬ
                                            </Button>
                                        </div>
                                    </Col>
                                </Row>
                            </FormComponent>
                        )}
                    />
                </Col>
            </Row>
        </SimplePage>
    );
};

export default AddNewBug;

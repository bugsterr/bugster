import React, { useEffect, useState } from 'react';

import { Button, Col, Row } from 'antd';
import Text from 'antd/lib/typography/Text';
import { Input } from 'Elements/Form';
import { GuildItem } from 'Elements/GuildItem';
import Typography from 'Elements/Typography/Typography';
import { PlusOutlined } from '@ant-design/icons';
import { GuildsService } from 'api/guilds-service';
import { Guild } from 'common';

const AllGuilds: React.FunctionComponent = () => {
    const [name, setName] = useState('');
    const [guilds, setGuilds] = useState<Guild[]>();

    useEffect(() => {
        GuildsService.getList().then((data) => {
            setGuilds(data);
        });
    }, []);

    const create = async () => {
        if (name) {
            GuildsService.create({ guildName: name }).then(() => {
                window.location.reload();
            });
        } else {
            console.log('Guild name is empty');
        }
    };

    return (
        <Row gutter={[8, 8]}>
            <Col span={12}>
                <Typography size="h3" bottom={18}>
                    Топ гильдий
                </Typography>
                {guilds?.map((guild) => {
                    return <GuildItem guild={guild} />;
                })}
            </Col>
            <Col span={12}>
                <Typography size="h3" bottom={18}>
                    Создание гильдий
                </Typography>
                <Text style={{ color: '#90ACB5' }}>
                    Объединяясь в гильдии, вы можете вести совместную работу над большим объёмом задач, делиться опытом
                    и достигать больше результатов в тестировании наших продуктов.
                </Text>
                <Input
                    style={{ marginTop: 22, border: '1px solid #27BEC9', borderRadius: 4 }}
                    placeholder="Название гильдии"
                    onChange={(e) => setName(e.target.value)}
                    value={name}
                />
                <Button type="primary" icon={<PlusOutlined />} className="Guild-create" onClick={create}>
                    СОЗДАТЬ ГИЛЬДИЮ
                </Button>
            </Col>
        </Row>
    );
};

export default AllGuilds;

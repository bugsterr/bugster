import React, { useEffect, useState } from 'react';

import { Button, Col, Row, Skeleton } from 'antd';

import './Guilds.scss';

import GuildUser from 'Elements/GuildUser/GuildUser';
import DiscordIcon from 'Elements/Icons/DiscordIcon';
import Typography from 'Elements/Typography/Typography';
import { useUsers } from 'Users';
import { GuildsService } from 'api/guilds-service';
import { Bug, Guild } from 'common';
import BugItem from 'Elements/BugItem/BugItem';
import AllGuilds from './AllGuilds';

const GuildsPage = () => {
    const [loaded, setLoaded] = useState(false);
    const [myGuild, setMyGuild] = useState<{ guild: Guild; bugs: Bug[] }>();
    const { getUser, myUser } = useUsers();

    useEffect(() => {
        if (myUser) {
            GuildsService.getMyGuild({ userId: myUser._id })
                .then((data) => {
                    setMyGuild(data);
                    setLoaded(true);
                })
                .catch(() => {
                    setLoaded(true);
                });
        }
    }, [myUser]);

    if (!loaded)
        return (
            <React.Fragment>
                <Typography size="h3" bottom={18}>
                    {!myGuild ? 'Топ гильдий' : 'Моя гильдия'}
                </Typography>
                <Row gutter={[8, 8]}>
                    {[...Array(2)].map((e, i) => (
                        <Col span={12} key={i}>
                            <Skeleton.Button active style={{ width: '100%', height: 110 }} />
                        </Col>
                    ))}
                </Row>
            </React.Fragment>
        );

    return (
        <React.Fragment>
            {!myGuild ? (
                <AllGuilds />
            ) : (
                <>
                    <Row gutter={[8, 8]}>
                        <Col span={12}>
                            <Typography size="h3" bottom={18}>
                                Моя гильдия
                            </Typography>
                            <Typography size="h3" bottom={18}>
                                {myGuild.guild.name}
                            </Typography>
                        </Col>
                        <Col span={12}>
                            <Button
                                type="primary"
                                icon={<DiscordIcon fill="white" style={{ marginRight: 4 }} />}
                                className="Guild-discord"
                            >
                                Вступить В Discord
                            </Button>
                        </Col>
                    </Row>
                    <Row gutter={[8, 8]} style={{ marginTop: 21 }}>
                        <Col span={8}>
                            <Typography size="h4" bottom={18}>
                                {getUser(myGuild.guild.creator)?.username}
                            </Typography>
                            <Row style={{ marginTop: 21 }}>
                                {myGuild.guild.members.map((member, index) => {
                                    return (
                                        <GuildUser id={member} description="Сотрудник банка «Открытие»" key={index} />
                                    );
                                })}
                            </Row>
                        </Col>
                        <Col span={16} style={{ paddingLeft: 10 }}>
                            <Typography
                                size="body2"
                                style={{ fontWeight: 900, textTransform: 'uppercase' }}
                                className="dark-blue-color"
                            >
                                {/* <BugOutlined style={{ marginRight: 10 }} /> */}
                                Последние баги
                            </Typography>
                            {myGuild.bugs.map((bug) => (
                                <BugItem bug={bug} />
                            ))}
                        </Col>
                    </Row>
                </>
            )}
        </React.Fragment>
    );
};

export default GuildsPage;

import React from 'react';
import { Row, Col, Skeleton, Alert } from 'antd';
import Typography from 'Elements/Typography/Typography';
import { getMyBugs } from 'api/bugs-service';
import BugItem from 'Elements/BugItem/BugItem';
import { useData } from 'api/use-data';

const History = () => {
    const data = useData(getMyBugs, {});

    return (
        <React.Fragment>
            <Typography size="h3" bottom={18}>
                История
            </Typography>
            {data.map({
                loading: () => (
                    <Row gutter={[8, 8]}>
                        {[...Array(4)].map((e, i) => (
                            <Col span={24} key={i}>
                                <Skeleton.Button active style={{ width: '100%', height: 110 }} />
                            </Col>
                        ))}
                    </Row>
                ),
                error: (error) => (
                    <Row gutter={[0, 8]}>
                        <Alert message={error} type="error" />
                    </Row>
                ),
                success: (bugs) => {
                    return bugs && bugs.length > 0 ? (
                        bugs.map((bug) => {
                            return (
                                <Row gutter={[8, 8]}>
                                    <BugItem key={bug._id} bug={bug} />
                                </Row>
                            );
                        })
                    ) : (
                        <Row gutter={[0, 8]}>
                            <div>Самое время найти свой первый баг 😊</div>
                        </Row>
                    );
                },
            })}
        </React.Fragment>
    );
};

export default History;

import React, { useEffect, useState } from 'react';
import { Row, Col, Skeleton } from 'antd';
import SafetyOutlined from '@ant-design/icons/SafetyOutlined';

import Typography from 'Elements/Typography/Typography';
import Progress from 'Elements/Progress/Progress';
import BananaIcon from 'Elements/Icons/BananaIcon';
import PlayerIcon from 'Elements/Icons/PlayerIcon';
import { declOfNum } from 'declOfNum';

import './User.scss';
import { UsersService } from 'api/users-service';
import { Experience } from 'common';
import { useUsers } from 'Users';

interface UserExperience {
    design: Experience;
    functional: Experience;
    vulnerability: Experience;
}

const User = () => {
    const [experiences, setExperiences] = useState<UserExperience>();

    const { myUser } = useUsers();

    useEffect(() => {
        if (myUser) {
            const designPromise = UsersService.getExp({ userId: myUser._id, type: 'design' });
            const funcPromise = UsersService.getExp({ userId: myUser._id, type: 'functional' });
            const vulnPromise = UsersService.getExp({ userId: myUser._id, type: 'vulnerability' });
            const promises = [designPromise, funcPromise, vulnPromise];

            myUser &&
                Promise.all(promises.map((p) => p.catch((e) => e))).then((val) => {
                    const exps: UserExperience = {
                        design: val[0],
                        functional: val[1],
                        vulnerability: val[2],
                    };
                    setExperiences(exps);
                });
        }
    }, [myUser]);

    const lvlValue = (exp: Experience) => {
        if (exp) {
            const lvl = Math.floor(exp.value / 3) + 1;
            if (lvl >= 1) return lvl;
            return 0;
        }
        return 0;
    };

    const expValue = (exp: Experience) => {
        if (exp) {
            const value = exp.value % 3;
            return value * 25;
        }
        return 0;
    };

    const designLvl = (experiences && lvlValue(experiences.design)) || 1;
    const vulnLvl = (experiences && lvlValue(experiences.vulnerability)) || 1;
    const funcLvl = (experiences && lvlValue(experiences.functional)) || 1;

    const coins = myUser && experiences ? designLvl + vulnLvl + funcLvl - myUser.coinsSpent - 3 : 0;

    return (
        <React.Fragment>
            <Row style={{ marginBottom: 20 }}>
                <Col span={24}>
                    <Row justify="center">
                        <PlayerIcon />
                    </Row>
                </Col>
                <Col span={24} style={{ margin: '20px 0' }}>
                    <Row align="middle" gutter={[10, 0]} style={{ flexDirection: 'column' }}>
                        {myUser ? (
                            <Typography size="h1">
                                {myUser.username}
                                <SafetyOutlined className="light-blue-color" style={{ fontSize: 26 }} />
                            </Typography>
                        ) : (
                            <Skeleton.Input style={{ width: '140px' }} />
                        )}

                        {myUser ? (
                            <Typography size="body2" className="dark-blue-color">
                                {myUser.fullName}
                            </Typography>
                        ) : (
                            <Skeleton.Input className="mt-8" size="small" style={{ width: '180px' }} />
                        )}
                        {myUser && (
                            <Typography size="body2" className="tile-blue-color">
                                Сотрудник банка «Открытие»
                            </Typography>
                        )}
                    </Row>
                </Col>
            </Row>
            <Row gutter={[18, 0]} style={{ marginBottom: 20 }}>
                <Col>
                    <BananaIcon />
                </Col>
                <Col flex="auto">
                    <Typography size="h4">Кошелёк</Typography>
                    <Typography size="h3" style={{ fontWeight: 900 }}>
                        {coins}&nbsp;{declOfNum(coins, ['балл', 'балла', 'баллов'])}
                    </Typography>
                </Col>
            </Row>
            <Progress
                title="Функционал"
                level={funcLvl}
                experience={(experiences && expValue(experiences.functional)) || 0}
                total_experience={75}
                gradient="blue"
            />
            <Progress
                title="Уязвимость"
                level={vulnLvl}
                experience={(experiences && expValue(experiences.vulnerability)) || 0}
                total_experience={75}
                gradient="red"
            />
            <Progress
                title="Дизайн"
                level={designLvl}
                experience={(experiences && expValue(experiences.design)) || 0}
                total_experience={75}
                gradient="pink"
            />
        </React.Fragment>
    );
};

export default User;

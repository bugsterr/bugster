import React from 'react';
import { Row, Col, Skeleton, Alert } from 'antd';

import Typography from 'Elements/Typography/Typography';
import BugItem from 'Elements/BugItem/BugItem';
import { getMyBugs } from 'api/bugs-service';
import { useData } from 'api/use-data';

const MainPage = () => {
    const data = useData(getMyBugs, { amount: 4 });

    return (
        <React.Fragment>
            <Typography size="h3" bottom={18}>
                Мои последние баги
            </Typography>
            {data.map({
                loading: () => (
                    <Row gutter={[8, 8]}>
                        {[...Array(2)].map((e, i) => (
                            <Col span={12} key={i}>
                                <Skeleton.Button active style={{ width: '100%', height: 110 }} />
                            </Col>
                        ))}
                    </Row>
                ),
                error: (error) => (
                    <Row gutter={[0, 8]}>
                        <Alert message={error} type="error" />
                    </Row>
                ),
                success: (bugs) => {
                    return bugs && bugs.length > 0 ? (
                        <Row gutter={[8, 8]}>
                            <Col span={12}>
                                {bugs[0] && <BugItem key={bugs[0]._id} bug={bugs[0]} />}
                                {bugs[2] && <BugItem key={bugs[2]._id} bug={bugs[2]} />}
                            </Col>
                            <Col span={12}>
                                {bugs[1] && <BugItem key={bugs[1]._id} bug={bugs[1]} />}
                                {bugs[3] && <BugItem key={bugs[3]._id} bug={bugs[3]} />}
                            </Col>
                        </Row>
                    ) : (
                        <Row gutter={[0, 8]}>
                            <div>Самое время найти свой первый баг 😊</div>
                        </Row>
                    );
                },
            })}
        </React.Fragment>
    );
};

export default MainPage;

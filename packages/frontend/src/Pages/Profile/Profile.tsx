import React from 'react';
import './Profile.scss';
import { Link } from 'react-router-dom';

import { BookOutlined, ReadOutlined, PlusOutlined } from '@ant-design/icons';
import { Button } from 'antd';

import TabPane from 'Elements/TabPane/TabPane';
import Page from 'Elements/Page/Page';

import Main from './Main';
import History from './History';
import User from './User';
import Guildes from './Guilds';
const Profile = () => {
    return (
        <Page
            title="Профиль"
            tabs={[
                <TabPane icon={<BookOutlined />} key="main">
                    Главная
                </TabPane>,
                <TabPane icon={<ReadOutlined />} key="history">
                    История
                </TabPane>,
                <TabPane icon={<BookOutlined />} key="team">
                    Моя гильдия
                </TabPane>,
            ]}
            right={
                <Link to="add-new-bug">
                    <Button type="primary" icon={<PlusOutlined />}>
                        НОВЫЙ БАГ
                    </Button>
                </Link>
            }
            sidebar={<User />}
            main={[
                { key: 'main', render: <Main /> },
                { key: 'history', render: <History /> },
                { key: 'team', render: <Guildes /> },
            ]}
        />
    );
};

export default Profile;

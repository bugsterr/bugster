import React, { useCallback } from 'react';

import SimplePage from 'Elements/SimplePage/SimplePage';
import Typography from 'Elements/Typography/Typography';
import Product from 'Elements/Product/Product';

import './Shop.scss';
import { useData } from 'api/use-data';
import { ShopService } from 'api/shop-service';
import times from 'lodash/times';
import { Skeleton } from 'antd';

const Shop = () => {
    const data = useData(ShopService.getShopData);

    const buy = useCallback((productId: string) => {
        console.log('Buy', productId);
        return ShopService.buy({ product: productId }).then(() => {});
    }, []);

    return (
        <SimplePage title="Магазин наград">
            <Typography size="h3" className="mt-40 mb-20">
                Аксессуары
            </Typography>
            <div className="Shop-products mb-72">
                {data.map({
                    loading: () =>
                        times(8, () => (
                            <div className="skeleton-wrapper">
                                <Skeleton.Button active style={{ width: '100%', height: '20vh' }} />
                                <Skeleton.Button
                                    active
                                    style={{ width: '50%', height: '20px', margin: '10px auto', display: 'block' }}
                                />
                                <Skeleton.Button
                                    active
                                    style={{ width: '80px', height: '30px', margin: '0 auto', display: 'block' }}
                                />
                            </div>
                        )),
                    error: () => null,
                    success: ({ products }) => {
                        return products.map((product) => <Product product={product} buy={buy} />);
                    },
                })}
            </div>
            <Typography size="h3" className="mt-40 mb-20">
                Мои покупки
            </Typography>
            <div className="Shop-products mb-72">
                {data.map({
                    loading: () => null,
                    error: () => null,
                    success: ({ entries }) => {
                        return entries.map(([, product]) => <Product product={product} />);
                    },
                })}
            </div>
        </SimplePage>
    );
};

export default Shop;

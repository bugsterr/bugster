import React from 'react';
import './AuthourizePage.scss';
import { FormComponent, FieldInput as Input } from 'Elements/Form';
import Typography from 'Elements/Typography/Typography';
import { Row, Col, Button } from 'antd';
import { Form } from 'react-final-form';
import { loginUser, registerUser } from 'api/users-service';
import { FORM_ERROR } from 'final-form';

const AuthourizePage = () => {
    const onSubmitLogin = async (values: { username: string; remember: boolean }) =>
        await loginUser(values.username, values.remember)
            .then(() => {
                window.location.reload();
            })
            .catch((err) => ({ [FORM_ERROR]: err }));

    const onSubmitRegister = async (values: { username: string; fullName: string; remember: boolean }) =>
        await registerUser({ username: values.username, fullName: values.fullName }, values.remember)
            .then(() => {
                window.location.reload();
            })
            .catch((err) => ({ [FORM_ERROR]: err }));

    return (
        <div className="AuthorizeForm">
            <div className="AuthorizeForm-container">
                <Row gutter={[50, 0]}>
                    <Col span={12} style={{ borderRight: '1px solid #333' }}>
                        <Form
                            onSubmit={onSubmitLogin}
                            subscription={{ submitError: true, submitting: true }}
                            render={({ handleSubmit, submitError, submitting }) => (
                                <FormComponent onSubmit={handleSubmit}>
                                    <Typography size="h1" className="mb-16">
                                        Вход
                                    </Typography>
                                    <Input name="username" placeholder="Логин" className="mb-16" />
                                    <Button type="primary" onClick={handleSubmit}>
                                        ВОЙТИ
                                    </Button>
                                </FormComponent>
                            )}
                        />
                    </Col>

                    <Col span={12}>
                        <Form
                            onSubmit={onSubmitRegister}
                            subscription={{ submitError: true, submitting: true }}
                            render={({ handleSubmit, submitError, submitting }) => (
                                <FormComponent onSubmit={handleSubmit}>
                                    <Typography size="h1" className="mb-16">
                                        Регистрация
                                    </Typography>
                                    <Input name="username" placeholder="Логин" className="mb-16" />
                                    <Input name="fullName" placeholder="Полное имя" className="mb-16" />
                                    <Button type="primary" onClick={handleSubmit}>
                                        ПРОДОЛЖИТЬ
                                    </Button>
                                </FormComponent>
                            )}
                        />
                    </Col>
                </Row>
            </div>
        </div>
    );
};

export default AuthourizePage;

export const bugTypes = [
    {
        value: 'design',
        title: 'Дизайн',
    },
    {
        value: 'functional',
        title: 'Функционал',
    },
    {
        value: 'vulnerability',
        title: 'Уязвимость',
    },
];

export type SelectData = {
    placeholder: string;
    options: {
        value: string;
        label: string;
    }[];
};

export const testedApp = {
    mobile: 'Мобильный банк',
    web: 'Веб-сайт',
    online: 'Онлайн-банк',
} as Record<string, string>;

export const deviceInfo1 = {
    mobile: {
        placeholder: 'Операционная система',
        options: [
            { value: 'android', label: 'Android' },
            { value: 'ios', label: 'iOS' },
        ],
    },
    web: {
        placeholder: 'Операционная система',
        options: [
            { value: 'windows', label: 'Windows' },
            { value: 'mac', label: 'Mac' },
            { value: 'linux', label: 'Linux' },
        ],
    },
    online: {
        placeholder: 'Операционная система',
        options: [
            { value: 'windows', label: 'Windows' },
            { value: 'mac', label: 'Mac' },
            { value: 'linux', label: 'Linux' },
        ],
    },
} as Record<string, SelectData>;

export type SelectData2 = {
    placeholder: string;
} & (
    | {
          type: 'input';
      }
    | {
          type: 'dropdown';
          options: Record<
              string,
              {
                  value: string;
                  label: string;
              }[]
          >;
      }
);

export const deviceInfo2 = {
    mobile: {
        placeholder: 'Устройство',
        type: 'input',
    },
    web: {
        placeholder: 'Браузер',
        type: 'dropdown',
        options: {
            windows: [
                { value: 'chrome', label: 'Chrome' },
                { value: 'firefox', label: 'Firefox' },
            ],
            mac: [{ value: 'safari', label: 'Safari' }],
            linux: [
                { value: 'chrome', label: 'Chrome' },
                { value: 'firefox', label: 'Firefox' },
            ],
        },
    },
} as Record<string, SelectData2>;

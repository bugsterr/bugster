import React, { Component, useContext } from 'react';
import cookie from 'react-cookies';

import { UsersService } from './api/users-service';
import { User } from 'common';

interface UsersProviderState {
    users: Map<string, User | null>; // null means loading
    myUser: User | null;
    loadUser(userId: string): void;
}

const UsersContext = React.createContext<UsersProviderState>({
    users: new Map(),
    myUser: null,
    loadUser: (_: string) => void 0,
});

class UsersProvider extends Component<Record<string, unknown>, UsersProviderState> {
    constructor(props: Record<string, unknown>) {
        super(props);
        this.loadUser = this.loadUser.bind(this);
        this.state = {
            users: new Map(),
            myUser: null,
            loadUser: this.loadUser,
        };
    }

    private usersLoading: Set<string> = new Set();

    loadUser(userId: string) {
        if (this.usersLoading.has(userId)) return;

        this.usersLoading.add(userId);

        Promise.resolve(() => {
            this.setState((state) => {
                const nextUsers = new Map(state.users);
                nextUsers.set(userId, null);
                return { users: nextUsers };
            });
        })
            .then(() => UsersService.get({ id: userId }))
            .then((user) => {
                this.setState((state) => {
                    const nextUsers = new Map(state.users);
                    nextUsers.set(userId, user);
                    return { users: nextUsers };
                });
                this.usersLoading.delete(userId);
            })
            .catch((error) => {
                console.error(error);
                this.usersLoading.delete(userId);
            });
    }

    componentDidMount() {
        const token = cookie.load('jwt-key');

        if (token) {
            UsersService.getMe()
                .then((myUser) => {
                    this.setState({ myUser });
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }

    render() {
        return <UsersContext.Provider value={this.state}>{this.props.children}</UsersContext.Provider>;
    }
}

const useUsers = () => {
    const { users, loadUser, myUser } = useContext(UsersContext);

    const getUser = (userId: string) => {
        if (!users.has(userId)) {
            loadUser(userId);
        }
        return users.get(userId) ?? null;
    };

    return {
        getUser,
        myUser,
    };
};

export { UsersProvider, useUsers };

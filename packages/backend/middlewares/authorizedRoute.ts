import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';

const accessTokenSecret = process.env.JWT;

const authorizedRoute = (req: Request, res: Response, next: NextFunction): void | Response<unknown> => {
    if (!accessTokenSecret) {
        return res.sendStatus(500);
    }

    try {
        const token = (<string>req.headers['authorization']).split(' ')[1];
        const userId = <{ id: string }>jwt.verify(token, accessTokenSecret);
        res.locals.userId = userId.id;
    } catch (error) {
        res.status(401).send('Authorization failed');
        return;
    }

    next();
};

export default authorizedRoute;

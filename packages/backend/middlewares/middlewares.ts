import express from 'express';
import cors from 'cors';
import mainRoutes from '../routes';
import userRoutes from '../Users/routes/userRoutes';
import bugRoutes from '../Bugs/routes/bugRoutes';
import shopRoutes from '../Shop/routes/shopRoutes';
import guildRoutes from '../Guilds/routes/guildRoutes';

const middleware = express();
const routes = [mainRoutes, userRoutes, bugRoutes, shopRoutes, guildRoutes];

export const middlewares = [
    middleware.use(
        cors({
            origin: 'http://localhost:3000',
        }),
    ),
    middleware.use(routes),
    middleware.use(express.json({ type: '*/*' })),
];

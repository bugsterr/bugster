import { Request, Response, NextFunction } from 'express';

// I'm terribly sorry for this, but express typing is not that good
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Fn = (request: Request, response: Response, next: NextFunction) => Promise<Response<any>>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const asyncHandler = (fn: Fn) => (req: Request, res: Response, next: NextFunction): Promise<void | Response<any>> => {
    return Promise.resolve(fn(req, res, next)).catch(next);
};

export default asyncHandler;

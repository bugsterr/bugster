import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import { UserModel } from '../Users/models/users';

dotenv.config({ path: './.env' });

const accessTokenSecret = process.env.JWT;

const adminRoute = async (req: Request, res: Response, next: NextFunction): Promise<void | Response<unknown>> => {
    if (!accessTokenSecret) {
        return res.sendStatus(500);
    }

    try {
        const token = (<string>req.headers['authorization']).split(' ')[1];
        const userId = <{ id: string }>jwt.verify(token, accessTokenSecret);
        const user = await UserModel.findOne({ _id: userId.id });
        if (!user?.get('isAdmin')) {
            res.status(401).send();
            return;
        }
    } catch (error) {
        res.status(401).send('Only admins allowed here');
        return;
    }

    next();
};

export default adminRoute;

import { Router } from 'express';

const router = Router();

router.get('/', function (request, response) {
    response.send('Ready to catch some bugs? 👀');
});

export default router;

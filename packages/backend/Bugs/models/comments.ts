import { Schema, Document, model } from 'mongoose';
import { Comment } from 'common';

const CommentSchema = new Schema({
    text: {
        type: String,
        required: true,
    },
    creationDate: {
        type: Date,
        required: true,
    },
    creator: {
        type: String,
        required: true,
    },
    relatedBug: {
        type: String,
        required: true,
    },
});

CommentSchema.set('toJSON', {
    virtuals: true,
});

export interface IComment extends Omit<Comment, '_id'>, Document {}
export const CommentModel = model<IComment>('Comment', CommentSchema);

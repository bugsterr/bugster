import { Schema, Document, model } from 'mongoose';
import { Bug } from 'common';

const BugSchema = new Schema({
    title: {
        type: String,
        required: true,
        createIndexes: true,
    },
    description: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
    },
    testedApp: {
        type: String,
        required: true,
    },
    appVersion: {
        type: String,
        required: true,
    },
    deviceInfo: {
        type: String,
        required: true,
    },
    creationDate: {
        type: Date,
        required: true,
    },
    creator: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        enum: ['reviewing', 'rejected', 'accepted'],
        default: 'reviewing',
    },
    comments: {
        type: Number,
        default: 0,
    },
});

BugSchema.set('toJSON', {
    virtuals: true,
});

export interface IBug extends Omit<Bug, '_id'>, Document {}
export const BugModel = model<IBug>('Bug', BugSchema);

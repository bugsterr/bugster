import { Schema, Document, model } from 'mongoose';
import { Attachment } from 'common';

const AttachmentSchema = new Schema({
    attachments: {
        type: [Buffer],
        required: true,
    },
    relatedBug: {
        type: String,
        required: true,
    },
    owner: {
        type: String,
        required: true,
    },
});

AttachmentSchema.set('toJSON', {
    virtuals: true,
});

export interface IAttachment extends Omit<Attachment, '_id'>, Document {}
export const AttachmentModel = model<IAttachment>('Attachment', AttachmentSchema);

import { Schema, Document, model } from 'mongoose';
import { Template } from 'common';

const TemplateSchema = new Schema({
    testedApp: {
        type: String,
        required: true,
    },
    appVersion: {
        type: String,
        required: true,
    },
    deviceInfo: {
        type: String,
        required: true,
    },
    creator: {
        type: String,
        required: true,
    },
});

TemplateSchema.set('toJSON', {
    virtuals: true,
});

export interface ITemplate extends Omit<Template, '_id'>, Document {}
export const TemplateModel = model<ITemplate>('Template', TemplateSchema);

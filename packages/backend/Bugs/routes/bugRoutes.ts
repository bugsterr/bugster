import { Router, Request, Response } from 'express';
import express from 'express';
import { createBug, resolveBug, findBugs, comment, getComments, getAttachment, getTemplates } from '../services/bugService';
import authorizedRoute from '../../middlewares/authorizedRoute';
import asyncHandler from '../../middlewares/asyncHandler';
import adminRoute from '../../middlewares/adminRoute';

const router = Router();
const toJSON = express.json();
const toJSONLarge = express.json({ limit: '100mb' });

router.post(
    '/bugs/create',
    [authorizedRoute, toJSONLarge],
    asyncHandler(async (req: Request, res: Response) => await createBug(req, res)),
);

router.post(
    '/bugs/resolve',
    [adminRoute, toJSON],
    asyncHandler(async (req: Request, res: Response) => await resolveBug(req, res)),
);

router.post(
    '/bugs/find',
    [authorizedRoute, toJSON],
    asyncHandler(async (req: Request, res: Response) => await findBugs(req, res)),
);

router.post(
    '/bugs/comment',
    [authorizedRoute, toJSON],
    asyncHandler(async (req: Request, res: Response) => await comment(req, res)),
);

router.post(
    '/bugs/get-comments',
    [authorizedRoute, toJSON],
    asyncHandler(async (req: Request, res: Response) => await getComments(req, res)),
);

router.post(
    '/bugs/get-attachment',
    [authorizedRoute, toJSON],
    asyncHandler(async (req: Request, res: Response) => await getAttachment(req, res)),
);

router.post(
    '/bugs/get-templates',
    [authorizedRoute, toJSON],
    asyncHandler(async (req: Request, res: Response) => await getTemplates(req, res)),
);

export default router;

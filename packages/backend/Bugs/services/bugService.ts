import { Request, Response } from 'express';
import { ExperienceModel } from '../../Users/models/experience';
import { UserModel } from '../../Users/models/users';
import { AttachmentModel, IAttachment } from '../models/attachments';
import { BugModel, IBug } from '../models/bugs';
import { CommentModel, IComment } from '../models/comments';
import { TemplateModel, ITemplate } from '../models/templates';

// Create new bug
export const createBug = async (request: Request, response: Response): Promise<Response<IBug>> => {
    const title = request.body.title as string;
    const description = request.body.description as string;
    const type = request.body.type as string;
    const testedApp = request.body.testedApp as string;
    const appVersion = request.body.appVersion as string;
    const deviceInfo = request.body.deviceInfo as string;
    const attachments = request.body.attachments as Buffer[] | undefined;
    const saveTemplate = request.body.template as boolean | undefined;

    const bug = new BugModel({
        title: title,
        description: description,
        type: type,
        testedApp: testedApp,
        appVersion: appVersion,
        deviceInfo: deviceInfo,
        creationDate: Date.now(),
        creator: response.locals.userId,
    });

    await bug.save();

    if (attachments) {
        const attachment = new AttachmentModel({
            attachments: attachments,
            relatedBug: bug._id,
            owner: response.locals.userId,
        });
        await attachment.save();
    }

    // If needed - save bug template for further usage
    if (saveTemplate) {
        const template = new TemplateModel({
            testedApp: testedApp,
            appVersion: appVersion,
            deviceInfo: deviceInfo,
            creator: response.locals.userId,
        });
        await template.save();
    }

    return response.send(bug);
};

// Resolve a bug: status=true for approve, status=false for decline
export const resolveBug = async (request: Request, response: Response): Promise<Response<IBug>> => {
    const bugId = request.body.id as string;
    const status = request.body.status as boolean;
    const bug = await BugModel.findByIdAndUpdate(bugId, { status: status ? 'accepted' : 'rejected' });

    if (!bug) {
        return response.status(404).send('Bug not found');
    }

    // Can't resolve a bug twice
    if (bug.status !== 'reviewing') {
        return response.status(400).send('Bug already resolved');
    }

    // Add experience to user in case bug is approved
    if (status) {
        const experience = await ExperienceModel.findOne({ userId: bug.creator, type: bug.type });
        if (experience) {
            experience.value += 1;
            await experience.save();
        } else {
            const newExperience = new ExperienceModel({
                userId: bug.creator,
                type: bug.type,
                value: 1,
            });
            await newExperience.save();
        }
    }

    return response.send(bug);
};

// Find & get bugs
export const findBugs = async (request: Request, response: Response): Promise<Response<IBug[]>> => {
    const amount = request.body.amount as number;
    const offset = request.body.offset as number;

    const creator = request.body.creator as string | undefined;
    const type = request.body.type as string | undefined;
    const status = request.body.status as 'reviewing' | 'rejected' | 'accepted' | undefined;
    const testedApp = request.body.testedApp as string | undefined;

    const bugs = await BugModel.find({
        creator: creator ? creator : /.*/,
        type: type ? type : /.*/,
        status: status ? status : /.*/,
        testedApp: testedApp ? testedApp : /.*/,
    })
        .skip(offset)
        .limit(amount)
        .sort('-creationDate');
    return response.send(bugs);
};

// Comment on a bug
export const comment = async (request: Request, response: Response): Promise<Response<IComment | string>> => {
    const text = request.body.text as string | undefined;
    const userId = response.locals.userId as string;
    const relatedBug = request.body.relatedBug as string | undefined;

    const bug = await BugModel.findById(relatedBug);

    if (!bug) {
        return response.status(404).send('Bug not found');
    }

    const comment = new CommentModel({
        text: text,
        creator: userId,
        creationDate: Date.now(),
        relatedBug: relatedBug,
    });
    await comment.save();

    bug.comments += 1;
    await bug.save();

    return response.send(comment);
};

// Get comments on a bug
export const getComments = async (request: Request, response: Response): Promise<Response<IComment[]>> => {
    const relatedBug = request.body.relatedBug as string | undefined;
    const comments = await CommentModel.find({ relatedBug: relatedBug });
    return response.send(comments);
};

// Get bug attachments
export const getAttachment = async (request: Request, response: Response): Promise<Response<IAttachment | string>> => {
    const relatedBug = request.body.relatedBug as string | undefined;
    const bug = await BugModel.findById(relatedBug);

    if (!bug) {
        return response.status(404).send('Bug not found');
    }

    const requestUserId = response.locals.userId as string;
    const requestUser = await UserModel.findOne({ _id: requestUserId });

    // Only admin or user who created the bug can get attachment
    if (requestUserId === bug.creator || requestUser?.isAdmin) {
        const attachment = await AttachmentModel.findOne({ relatedBug: relatedBug });
        return response.send(attachment);
    }

    // Any other user can't recieve attachments
    return response.status(401).send('Insufficient permissions');
};

// Get templates created by user
export const getTemplates = async (request: Request, response: Response): Promise<Response<ITemplate | string>> => {
    const requestUserId = response.locals.userId as string;
    const templates = await TemplateModel.find({ creator: requestUserId });
    return response.send(templates);
};

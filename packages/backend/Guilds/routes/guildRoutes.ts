import express, { Router } from 'express';
import authorizedRoute from '../../middlewares/authorizedRoute';
import asyncHandler from '../../middlewares/asyncHandler';
import { create, join, list, myGuild } from '../services/guildsService';

const router = Router();
const toJSON = express.json();

router.post(
    '/guilds/create',
    [authorizedRoute, toJSON],
    asyncHandler(create),
);

router.post(
    '/guilds/join',
    [authorizedRoute, toJSON],
    asyncHandler(join),
);

router.post(
    '/guilds/list',
    [authorizedRoute, toJSON],
    asyncHandler(list)
);

router.post(
    '/guilds/my-guild',
    [authorizedRoute, toJSON],
    asyncHandler(myGuild),
);

export default router;

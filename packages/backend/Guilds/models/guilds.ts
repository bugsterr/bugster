import { Schema, Document, model } from 'mongoose';
import { Guild } from 'common';

const GuildSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    members: {
        type: [String],
        default: [],
    },
    creator: {
        type: String,
        required: true,
    },
    channel: {
        type: String,
        required: true,
    },
    hasMultiplier: {
        type: Boolean,
    }
});

GuildSchema.set('toJSON', {
    virtuals: true,
});

export interface IGuild extends Omit<Guild, '_id'>, Document {}
export const GuildModel = model<IGuild>('Guild', GuildSchema);

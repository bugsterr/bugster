import { Request, Response } from 'express';
import { GuildModel } from '../models/guilds';

import { client } from '../../discord';
import { Bug, Guild } from 'common';
import { BugModel } from '../../Bugs/models/bugs';

const serverId = process.env.DISCORD_SERVER_ID ?? '';

const categoryId = process.env.DISCORD_CATEGORY_ID ?? '';

// Create guild
export const create = async (request: Request, response: Response): Promise<Response<{ url: string } | string>> => {
    const userId = response.locals.userId as string;

    const guildName = request.body.guildName as string;

    if (!guildName.match(/^[a-z0-9_]+$/)) {
        return response.status(400).send('В названии гильдии могут быть только буквы a-z, цифры и знак _!');
    }

    const count = await GuildModel.countDocuments({ name: guildName });

    if (count > 0) {
        return response.status(400).send('Guild already exists');
    }

    const server = client.guilds.resolve(serverId);

    if (!server) {
        return response.status(500).send('Discord server not found!');
    }

    const channel = await server.channels.create(guildName, { type: 'text', parent: categoryId });

    await channel.send(`Добро пожаловать в канал гильдии ${guildName}!`);

    const guild = new GuildModel({
        name: guildName,
        creator: userId,
        channel: channel.id,
        members: [userId]
    });

    await guild.save();

    const invite = await channel.createInvite({ unique: true, maxUses: 1 });

    return response.send({ url: invite.url });
};

export const join = async (request: Request, response: Response): Promise<Response<{ url: string } | string>> => {
    const userId = response.locals.userId as string;

    const guildId = request.body.guildId as string;

    const guild = await GuildModel.findOne({ _id: guildId });

    if (!guild) {
        return response.status(404).send('User not found');
    }

    if (guild.members.includes(userId)) {
        return response.status(400).send('User already in guild');
    }

    const server = client.guilds.resolve(serverId);

    if (!server) {
        return response.status(500).send('Discord server not found!');
    }

    const channel = server.channels.resolve(guild.channel);

    if (!channel) {
        return response.status(500).send('Discord channel not found!');
    }

    const invite = await channel.createInvite({ unique: true, maxUses: 1 });

    guild.members = [...guild.members, userId];

    await guild.save();

    return response.send({ url: invite.url });
}

export const list = async (request: Request, response: Response): Promise<Response<Partial<Guild>[] | string>> => {
    const guilds = await GuildModel.find();

    const viewGuilds: Partial<Guild>[] = guilds.map(guild => ({
        _id: guild._id,
        name: guild.name,
        members: guild.members,
        creator: guild.creator,
        hasMultiplier: guild.hasMultiplier,
    }));

    return response.send(viewGuilds);
}

export const myGuild = async (request: Request, response: Response): Promise<Response<{ guild: Guild, bugs: Bug[] } | string>> => {
    const userId = response.locals.userId as string;
    const guild = await GuildModel.findOne({ members: userId });

    if (!guild) {
        return response.send('Guild is not found!');
    }

    const bugs = await BugModel.find({ 'creator': { '$in': guild.members } })
        .limit(4)
        .sort('-creationDate');

    return response.send({ guild, bugs });
}

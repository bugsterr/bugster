import { Client } from 'discord.js';

const client = new Client();

const token = process.env.DISCORD_TOKEN ?? undefined;

export { client, token }

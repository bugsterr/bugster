import { Schema, Document, model } from 'mongoose';
import { Order } from 'common';

const OrderSchema = new Schema({
    product: {
        type: String,
        required: true,
    },
    buyer: {
        type: String,
        required: true,
    },
});

OrderSchema.set('toJSON', {
    virtuals: true,
});

export interface IOrder extends Omit<Order, '_id'>, Document {}
export const OrderModel = model<IOrder>('Order', OrderSchema);

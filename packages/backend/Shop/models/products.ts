import { Schema, Document, model } from 'mongoose';
import { Product } from 'common';

const ProductSchema = new Schema({
    picture: {
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
});

ProductSchema.set('toJSON', {
    virtuals: true,
});

export interface IProduct extends Omit<Product, '_id'>, Document {}
export const ProductModel = model<IProduct>('Product', ProductSchema);

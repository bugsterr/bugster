import { Request, Response } from 'express';
import { ExperienceModel } from '../../Users/models/experience';
import { UserModel } from '../../Users/models/users';
import { IOrder, OrderModel } from '../models/orders';
import { IProduct, ProductModel } from '../models/products';

// Buy a product
export const buy = async (request: Request, response: Response): Promise<Response<IOrder | string>> => {
    const productId = request.body.product;
    const product = await ProductModel.findById(productId);

    if (!product) {
        return response.status(404).send('Product not found');
    }

    const requestUserId = response.locals.userId as string;
    const requestUser = await UserModel.findById(requestUserId);

    if (!requestUser) {
        return response.status(401).send('Insufficient permissions');
    }

    // Check if user has enough coins
    const exp = await ExperienceModel.find({ userId: requestUserId });
    let coins = 0;

    // Check how many coins user has
    exp.forEach((e) => {
        coins += Math.floor(e.value / 3);
    });
    coins -= requestUser.coinsSpent;

    if (coins < product.price) {
        return response.status(400).send('Not enough coins');
    }

    // Update user coins spent
    requestUser.coinsSpent += product.price;
    await requestUser.save();

    // Place a new order
    const order = new OrderModel({
        product: product._id,
        buyer: requestUserId,
    });
    await order.save();

    return response.send(order);
};

export const getProducts = async (request: Request, response: Response): Promise<Response<IProduct[]>> => {
    const products = await ProductModel.find().sort('price');
    return response.send(products);
};

export const getOrders = async (request: Request, response: Response): Promise<Response<IOrder[]>> => {
    const requestUserId = response.locals.userId as string;
    const orders = await OrderModel.find({ buyer: requestUserId });
    return response.send(orders);
};

import { Router, Request, Response } from 'express';
import express from 'express';
import authorizedRoute from '../../middlewares/authorizedRoute';
import asyncHandler from '../../middlewares/asyncHandler';
import { buy, getOrders, getProducts } from '../services/shopService';

const router = Router();
const toJSON = express.json();

router.post(
    '/shop/get-products',
    asyncHandler(async (req: Request, res: Response) => await getProducts(req, res)),
);

router.post(
    '/shop/get-orders',
    [authorizedRoute, toJSON],
    asyncHandler(async (req: Request, res: Response) => await getOrders(req, res)),
);

router.post(
    '/shop/buy',
    [authorizedRoute, toJSON],
    asyncHandler(async (req: Request, res: Response) => await buy(req, res)),
);

export default router;

import { Router, Request, Response } from 'express';
import express from 'express';
import { listUsers, registerUser, getUser, loginUser, getExperience, getMe } from '../services/userService';
import authorizedRoute from '../../middlewares/authorizedRoute';
import asyncHandler from '../../middlewares/asyncHandler';
import adminRoute from '../../middlewares/adminRoute';

const router = Router();
const toJSON = express.json();

router.post(
    '/users/list',
    adminRoute,
    asyncHandler(async (req: Request, res: Response) => await listUsers(req, res)),
);

router.post(
    '/users/me',
    authorizedRoute,
    asyncHandler(async (req: Request, res: Response) => await getMe(req, res)),
);

router.post(
    '/users/register',
    toJSON,
    asyncHandler(async (req: Request, res: Response) => await registerUser(req, res)),
);

router.post(
    '/users/get',
    [authorizedRoute, toJSON],
    asyncHandler(async (req: Request, res: Response) => await getUser(req, res)),
);

router.post(
    '/users/login',
    toJSON,
    asyncHandler(async (req: Request, res: Response) => await loginUser(req, res)),
);

router.post(
    '/users/get-exp',
    [authorizedRoute, toJSON],
    asyncHandler(async (req: Request, res: Response) => await getExperience(req, res)),
);

export default router;

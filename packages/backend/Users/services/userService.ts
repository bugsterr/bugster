import { Request, Response } from 'express';
import { IUser, UserModel } from '../models/users';
import * as jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import { ExperienceModel, IExperience } from '../models/experience';

dotenv.config({ path: './.env' });

const accessTokenSecret = process.env.JWT;

// Admin-only method for listing all users
export const listUsers = async (request: Request, response: Response): Promise<Response<IUser[]>> => {
    const users = await UserModel.find({});
    return response.send(users) as Response<IUser[]>;
};

// New users registration
export const registerUser = async (
    request: Request,
    response: Response,
): Promise<Response<string | { token: string }>> => {
    const username = request.body.username as string;
    const fullName = request.body.fullName as string | undefined;

    const existingUser = await UserModel.findOne({ username: username });
    if (existingUser) {
        return response.status(400).send('User already exists');
    }

    const user = new UserModel({
        username: username,
        fullName: fullName,
    });
    await user.save();

    if (accessTokenSecret !== undefined && user) {
        const accessToken = jwt.sign({ id: user._id }, accessTokenSecret);
        return response.json({ token: accessToken });
    }

    const err = 'Error registering user:\nInternal server error';
    console.log(err);
    return response.status(500).send(err);
};

// Logging in to existing accounts
export const loginUser = async (
    request: Request,
    response: Response,
): Promise<Response<string | { token: string }>> => {
    const username = request.body.username as string;
    const user = await UserModel.findOne({ username: username });

    if (accessTokenSecret !== undefined && user) {
        const accessToken = jwt.sign({ id: user._id }, accessTokenSecret);
        return response.json({ token: accessToken });
    }

    return response.status(401).send('User not found');
};

// Get user data by id
// Admin & user requesting their own data get full data (including personal info)
// All other users only get username & bug list
export const getUser = async (request: Request, response: Response): Promise<Response<string | IUser>> => {
    const userId = request.body.id as string;
    const user = await UserModel.findOne({ _id: userId });
    const requestUserId = response.locals.userId as string;
    const requestUser = await UserModel.findOne({ _id: requestUserId });

    if (!user) {
        return response.status(404).send('User not found');
    }

    // If a user gets their own data or admin gets someone's data, return full data
    if (requestUserId === userId || requestUser?.isAdmin) {
        return response.send(user);
    }

    // If user gets another user, return a subset of data
    return response.send({
        username: user.username,
        _id: userId,
    });
};

// Get user experience (amount of bugs closed)
export const getExperience = async (request: Request, response: Response): Promise<Response<IExperience | string>> => {
    const userId = response.locals.userId as string;
    const type = request.body.type as string | undefined;
    const experience = await ExperienceModel.findOne({ userId: userId, type: type ? type : /.*/ });

    if (!experience) {
        return response.status(404).send('User not found');
    }

    return response.send(experience);
};

// Get your profile
export const getMe = async (request: Request, response: Response): Promise<Response<IUser>> => {
    const userId = response.locals.userId as string;
    const user = await UserModel.findById(userId);
    return response.send(user);
};

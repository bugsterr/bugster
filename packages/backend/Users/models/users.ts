import { Schema, Document, model } from 'mongoose';
import { User } from 'common';

const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        createIndexes: true,
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    fullName: {
        type: String,
    },
    coinsSpent: {
        type: Number,
        default: 0,
    },
});

UserSchema.set('toJSON', {
    virtuals: true,
});

export interface IUser extends Omit<User, '_id'>, Document {}
export const UserModel = model<IUser>('User', UserSchema);

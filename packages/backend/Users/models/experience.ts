import { Schema, Document, model } from 'mongoose';
import { Experience } from 'common';

const ExperienceSchema = new Schema({
    userId: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
    },
    value: {
        type: Number,
        default: 0,
    },
});

ExperienceSchema.set('toJSON', {
    virtuals: true,
});

export interface IExperience extends Omit<Experience, '_id'>, Document {}
export const ExperienceModel = model<IExperience>('Experience', ExperienceSchema);
